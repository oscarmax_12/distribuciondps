package com.legado.distribuciongps.adapters;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.ArticuloRebote;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * SimpleDemoAdapter, just shows demo data
 */
public class ArticuloReboteAdapter extends SectioningAdapter {

    static final String TAG = ArticuloReboteAdapter.class.getSimpleName();
    static final boolean USE_DEBUG_APPEARANCE = false;

    public static class Section {
        int index;
        String header;
        BigDecimal montoSum;
        String footer;
        ArrayList<ArticuloRebote> items = new ArrayList<>();


    }

    public class ItemViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener {
        TextView txtArticulo;
        TextView txtUnidadMedida;
        TextView txtCantidad;
        TextView txtPaquete;
        TextView txtUnidades;
        TextView txtTotal;


        public ItemViewHolder(View itemView) {
            super(itemView);
            txtArticulo = (TextView) itemView.findViewById(R.id.txtArticulo);
            txtUnidadMedida = (TextView) itemView.findViewById(R.id.txtUnidadMedida);
            txtCantidad = (TextView) itemView.findViewById(R.id.txtCantidad);
            txtPaquete = (TextView) itemView.findViewById(R.id.txtPaquete);
            txtUnidades=(TextView)itemView.findViewById(R.id.txtUnidades);
            txtTotal = (TextView) itemView.findViewById(R.id.txtTotal);

            if (!ArticuloReboteAdapter.this.showAdapterPositions) {
                txtUnidadMedida.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            final int section = ArticuloReboteAdapter.this.getSectionForAdapterPosition(adapterPosition);
            final int item = ArticuloReboteAdapter.this.getPositionOfItemInSection(section, adapterPosition);

        }
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder implements View.OnClickListener {
        TextView textView;
        TextView adapterPositionTextView;
        ImageButton collapseButton;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.txtBienvenido);
            adapterPositionTextView = (TextView) itemView.findViewById(R.id.adapterPositionTextView);

            collapseButton = (ImageButton) itemView.findViewById(R.id.collapseButton);
            collapseButton.setOnClickListener(this);

            if (!ArticuloReboteAdapter.this.showCollapsingSectionControls) {
                collapseButton.setVisibility(View.GONE);
            }


        }

        void updateSectionCollapseToggle(boolean sectionIsCollapsed) {
            @DrawableRes int id = sectionIsCollapsed
                    ? R.drawable.ic_expand_more_black_24dp
                    : R.drawable.ic_expand_less_black_24dp;

            collapseButton.setImageDrawable(ContextCompat.getDrawable(collapseButton.getContext(), id));
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            final int section = ArticuloReboteAdapter.this.getSectionForAdapterPosition(position);
            if   (v == collapseButton) {
                ArticuloReboteAdapter.this.onToggleSectionCollapse(section);
                updateSectionCollapseToggle(ArticuloReboteAdapter.this.isSectionCollapsed(section));
            }
        }
    }

    public class FooterViewHolder extends SectioningAdapter.FooterViewHolder {
        TextView textView;
        TextView adapterPositionTextView;

        public FooterViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.txtBienvenido);
            adapterPositionTextView = (TextView) itemView.findViewById(R.id.adapterPositionTextView);

            if (!ArticuloReboteAdapter.this.showAdapterPositions) {
                adapterPositionTextView.setVisibility(View.GONE);
            }
        }
    }


    ArrayList<Section> sections = new ArrayList<>();
     boolean showCollapsingSectionControls;
    boolean showAdapterPositions;
    boolean hasFooters;

    public ArticuloReboteAdapter(List<List<ArticuloRebote>> listaAdapter,  boolean hasFooters, boolean showModificationControls, boolean showCollapsingSectionControls, boolean showAdapterPositions) {
         this.showCollapsingSectionControls = showCollapsingSectionControls;
        this.showAdapterPositions = showAdapterPositions;
        this.hasFooters = hasFooters;

        for (int i = 0; i < listaAdapter.size(); i++) {
            if(listaAdapter.get(i).size()>0){
                appendSection(i, listaAdapter.get(i));
            }

        }
        System.out.println(hasFooters);
    }

    void appendSection(int index, List<ArticuloRebote> listaArt) {
        Section section = new Section();
        section.index = index;
       section.header = listaArt.get(0).getTipoRebote();

        if (this.hasFooters) {
            section.footer = "End of section " + index;
        }

        BigDecimal suma=new BigDecimal(0);
        for (int j = 0; j < listaArt.size(); j++) {
            section.items.add(listaArt.get(j));
           // if(!listaArt.get(j).getCodigo().equals("000000")){
                suma=suma.add(listaArt.get(j).getTotal());
           // }

        }

        section.montoSum=suma;
        sections.add(section);
    }

    void onToggleSectionCollapse(int sectionIndex) {
        Log.d(TAG, "onToggleSectionCollapse() called with: " + "sectionIndex = [" + sectionIndex + "]");
        setSectionIsCollapsed(sectionIndex, !isSectionCollapsed(sectionIndex));
    }

    @Override
    public int getNumberOfSections() {
        return sections.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return sections.get(sectionIndex).items.size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return !TextUtils.isEmpty(sections.get(sectionIndex).header);
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return !TextUtils.isEmpty(sections.get(sectionIndex).footer);
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_item_simple_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_item_simple_header, parent, false);
        return new HeaderViewHolder(v);
    }

    @Override
    public FooterViewHolder onCreateFooterViewHolder(ViewGroup parent, int footerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_item_simple_footer, parent, false);
        return new FooterViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, int itemIndex, int itemType) {
        Section s = sections.get(sectionIndex);
        ItemViewHolder ivh = (ItemViewHolder) viewHolder;
        ivh.txtArticulo.setText(s.items.get(itemIndex).getCodigo()+"-"+s.items.get(itemIndex).getDescripcion());
//        Integer.toString(getAdapterPositionForSectionItem(sectionIndex, itemIndex))
        ivh.txtUnidadMedida.setText(s.items.get(itemIndex).getUnidadMedida());
        ivh.txtCantidad.setText(s.items.get(itemIndex).getCantidad().toString());
        ivh.txtPaquete.setText(s.items.get(itemIndex).getPaquetes().toString());
        ivh.txtUnidades.setText(s.items.get(itemIndex).getUnidades().toString());
        ivh.txtTotal.setText(s.items.get(itemIndex).getTotal().toString());

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        Section s = sections.get(sectionIndex);
        HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
        hvh.adapterPositionTextView.setText(s.montoSum.toString());

        if (USE_DEBUG_APPEARANCE) {
            hvh.textView.setText(pad(sectionIndex * 2) + s.header);
            viewHolder.itemView.setBackgroundColor(0x55FF9999);
        } else {
            hvh.textView.setText(s.header);
        }

        hvh.updateSectionCollapseToggle(isSectionCollapsed(sectionIndex));
    }

    @Override
    public void onBindGhostHeaderViewHolder(SectioningAdapter.GhostHeaderViewHolder viewHolder, int sectionIndex) {
        if (USE_DEBUG_APPEARANCE) {
            viewHolder.itemView.setBackgroundColor(0xFF9999FF);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindFooterViewHolder(SectioningAdapter.FooterViewHolder viewHolder, int sectionIndex, int footerType) {
        Section s = sections.get(sectionIndex);
        FooterViewHolder fvh = (FooterViewHolder) viewHolder;
        fvh.textView.setText(s.footer);
        fvh.adapterPositionTextView.setText(Integer.toString(getAdapterPositionForSectionFooter(sectionIndex)));
    }

    private String pad(int spaces) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < spaces; i++) {
            b.append(' ');
        }
        return b.toString();
    }

}
