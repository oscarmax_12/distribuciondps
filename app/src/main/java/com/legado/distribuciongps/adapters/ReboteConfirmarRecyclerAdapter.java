package com.legado.distribuciongps.adapters;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.fragments.InventarioReboteFragment;
import com.legado.distribuciongps.fragments.RebotesConfirmarFragment;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.AdapterCommunication;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.SessionUsuario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by __Adrian__ on 19/4/2017.
 */

public class ReboteConfirmarRecyclerAdapter extends RecyclerView.Adapter<ReboteConfirmarRecyclerAdapter.ReboteConfirmarRecyclerViewHolder> implements ItemEntregaListener {
    private static final String TAG = "EntregasRecyclerAdapter";

    private List<Rebote> documentoDeudas = new ArrayList<>();
    private FragmentActivity activity;
    private AdapterCommunication mListener;
    private Context context;
    private SessionUsuario sessionUsuario;
    Fragment fragment;
    private SwipeRefreshLayout swiperefresh;
    // MyDialogProgress dialogPago;
    InterfaceReboteInventarioFragment interfaceReboteInventarioFragment;

    Bundle args;

    public void setOnClickListener(AdapterCommunication listener) {
        mListener = listener;
    }


    public ReboteConfirmarRecyclerAdapter(List<Rebote> documentoDeudas, FragmentActivity activity, Fragment fragment, SwipeRefreshLayout swiperefresh, InterfaceReboteInventarioFragment interfaceReboteInventarioFragment) {
        this.activity = activity;
        this.documentoDeudas = documentoDeudas;
        this.fragment = fragment;
        this.interfaceReboteInventarioFragment = interfaceReboteInventarioFragment;
        this.swiperefresh = swiperefresh;
    }

    @Override
    public ReboteConfirmarRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mis_rebotes, parent, false);
        sessionUsuario = new SessionUsuario(v.getContext());
        return new ReboteConfirmarRecyclerViewHolder(v, this, swiperefresh);
    }

    @Override
    public void onBindViewHolder(final ReboteConfirmarRecyclerViewHolder holder, final int position) {
        holder.numeroDoc.setText(documentoDeudas.get(position).getSerie() + "-" + documentoDeudas.get(position).getPreimpreso());

        holder.monto.setText(documentoDeudas.get(position).getMonto().toString());
        holder.descCliente.setText(documentoDeudas.get(position).getDescCliente());
        holder.motivoRebote.setText(documentoDeudas.get(position).getTipoRebote());
        holder.itemView.setSelected(true);
        //holder.itemView.setBackgroundResource(R.drawable.selector_row_cobranza);
        holder.btnCambiarEntrega.setOnClickListener(view -> {

            boolean rpta = MensajesDialogAlert.mensajeConfirmacion(activity, "CONFIRMAR ENTREGA", "Desea Retornar a Entrega");

            if (rpta)

                retornarAEntrega(documentoDeudas.get(position), position);

        });


    }

    private void retornarAEntrega(Rebote rebote, int pos) {

        Gson gson = new Gson();

        rebote.setPlaca(sessionUsuario.getCodigoAplicacion());
        String logRepartoJson = gson.toJson(rebote);
        Log.d("assas", logRepartoJson);
        Log.wtf("DATOS RETORNAR", logRepartoJson);
        ////
        Call<JsonRespuesta<Integer>> call = ApiRetrofit.getInstance().getMetricasService().mantRetornarEntrega(rebote);
        call.enqueue(new Callback<JsonRespuesta<Integer>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Integer>> call, Response<JsonRespuesta<Integer>> response) {
                if (response.body().getEstado() == -1) {

                } else {
                    notifyItemRemoved(pos);
                    notifyDataSetChanged();
                    documentoDeudas.remove(pos);
                    notifyItemRemoved(pos);
                    notifyDataSetChanged();
                    notifyItemRangeChanged(pos, documentoDeudas.size());
                    swiperefresh.post(new Runnable() {
                        @Override
                        public void run() {
                            swiperefresh.setRefreshing(false);
                            ((RebotesConfirmarFragment) fragment).cargarRebotesByConfirmar();


                        }
                    });

                }

            }

            //
            @Override
            public void onFailure(Call<JsonRespuesta<Integer>> call, Throwable t) {

            }
        });
    }

    public BigDecimal montoTotalLiquidacion() {
        BigDecimal montoTotal = new BigDecimal(0);
        for (int i = 0; i < documentoDeudas.size(); i++) {
            montoTotal = montoTotal.add(documentoDeudas.get(i).getMonto());
        }
        return montoTotal;
    }

    public int cantTotal() {
//        BigDecimal montoTotal=new BigDecimal(0);
//        for (int i = 0; i <documentoDeudas.size() ; i++) {
//            montoTotal = montoTotal.add(documentoDeudas.get(i).getMonto());
//        }
        return documentoDeudas.size();
    }

    @Override
    public int getItemCount() {
        return documentoDeudas.size();
    }

    public List<Rebote> listaEntregas() {
        return documentoDeudas;
    }


    @Override
    public void onItemClick(View view, int position) {


    }

    public interface InterfaceReboteInventarioFragment {

        public void reloadInventario();
    }


    public static class ReboteConfirmarRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView numeroDoc;
        private TextView codCliente;
        private TextView descCliente;
        private TextView monto;
        private TextView motivoRebote;
        private TextView txtEstado;
        private TextView txtCodVendedor;
        private TextView txtDireccion;
        private ItemEntregaListener listener;
        private Button btnCambiarEntrega;
        private SwipeRefreshLayout swiperefresh;

        public ReboteConfirmarRecyclerViewHolder(View itemView, ItemEntregaListener listener, SwipeRefreshLayout swiperefresh) {
            super(itemView);
            numeroDoc = (TextView) itemView.findViewById(R.id.txtNumeroDoc);
            descCliente = (TextView) itemView.findViewById(R.id.txtDescCliente);
            monto = (TextView) itemView.findViewById(R.id.txtMonto);
            motivoRebote = (TextView) itemView.findViewById(R.id.textMotivoRebote);
            btnCambiarEntrega = (Button) itemView.findViewById(R.id.btnEntregar);

            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(view, getAdapterPosition());
        }
    }
}

interface ItemReboteConfirmarListener {

    void onItemClick(View view, int position);
}

