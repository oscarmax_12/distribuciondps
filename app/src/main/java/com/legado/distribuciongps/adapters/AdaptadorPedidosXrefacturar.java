package com.legado.distribuciongps.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Almacen;
import com.legado.distribuciongps.modelo.Cliente;
import com.legado.distribuciongps.modelo.Documento;
import com.legado.distribuciongps.modelo.Empresa;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;

public class AdaptadorPedidosXrefacturar extends RecyclerView.Adapter<AdaptadorPedidosXrefacturar.ViewHolder> {
    private Context context;
    private Activity act;
    private List<Pedido> listaPedidoAux;
    private int posicionItemSeleccionado;
    ProgressDialog progressDialog2;
    SessionUsuario sessionusuario;


    public AdaptadorPedidosXrefacturar(Context context,Activity act) {
        this.context = context;
        this.act = act;
        sessionusuario=new SessionUsuario(act);
        progressDialog2=new ProgressDialog(this.context);
    }

    public int obtenerPosicionItemSeleccionado() {
        return posicionItemSeleccionado;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pedido_xrefacturar, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.txtNroPedido.setText("Pedido: " + listaPedidoAux.get(position).getNumPedidoRef());
        holder.txtCliente.setText(listaPedidoAux.get(position).getCliente().getNomCliente() + " " + listaPedidoAux.get(position).getCliente().getCodCliente());
        holder.btnGenerarFactura.setOnClickListener(view -> {

            holder.GenerarFactura(listaPedidoAux.get(position));

        });

    }

    @Override
    public int getItemCount() {
        return listaPedidoAux.size();
    }

    public void addList(List<Pedido> arrayList) {
        //Permite recivir la lista de biens que esta dentro de la clase bien
        listaPedidoAux = arrayList;

        //Permite notificar al Adaptador que cambios en la base de datos
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnLongClickListener, View.OnCreateContextMenuListener {
        //Permite declarar y relacionar los controles que estan contenidos dentro del CardView
        TextView txtNroPedido, txtCliente;
        Button btnGenerarFactura;
        RecyclerView rvListaPorductos;

        public ViewHolder(View itemView) {
            super(itemView);

            //Relacionar (enlazar) los controles declarados en java con los controles que estan en el CardView
            txtNroPedido = (TextView) itemView.findViewById(R.id.txtPedidoXrefacturar);
            txtCliente = (TextView) itemView.findViewById(R.id.txtClienteXrefacturar);
            btnGenerarFactura = (Button) itemView.findViewById(R.id.btnGenerarRefacturar);


            itemView.setOnClickListener(this::onClick);
//            if (listaPedidoAux.size() <= 1) {
//                itemView.callOnClick();
//            }
            itemView.setOnLongClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
////            int position = getAdapterPosition();
////            String identificador = listaPackingAux.get(position).getIdentificador_s();
////            String marca = listaPackingAux.get(position).getMarca();
////           Log.e("dio clic",marca);
////
////            Intent obj=new Intent(context,DetalleBienAct.class);
////            obj.putExtra("cod",identificador);
////            context.startActivity(obj);
//            v.
//        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            String nombre = listaPackingAux.get(this.getAdapterPosition()).getNro_packing();
//
//            menu.setHeaderTitle(nombre);
//
//            menu.add(0, 1, 0, "Editar");
//            menu.add(0, 2, 0, "Cancelar");
//            menu.add(0, 3, 0, "Completado");
        }

        @Override
        public boolean onLongClick(View v) {
            posicionItemSeleccionado = this.getAdapterPosition();
            return false;
        }

        public void GenerarFactura(Pedido ped) {
            Gson gson = new Gson();
            String lcJson = gson.toJson(ped);
            Log.e("GSON PED", lcJson);

            Metodos.showProgressDialogWithTitle("REACTURANDO PEDIDO",progressDialog2);

            Call<JsonRespuesta<Documento>> call = ApiRetrofit.getInstance().getPedidoService().RefacturarPedido(ped);
            call.enqueue(new Callback<JsonRespuesta<Documento>>() {
                @Override
                public void onResponse(Call<JsonRespuesta<Documento>> call, retrofit2.Response<JsonRespuesta<Documento>> response) {
                    JsonRespuesta rptaClientes = response.body();
                    Integer estado = rptaClientes.getEstado();

                    if (estado == 1) {

                        Toast.makeText(context, "Pedido Facturado correctamente.", Toast.LENGTH_LONG).show();
                        Timer timer2 = new Timer();
                        TimerTask t2 = new TimerTask() {
                            @Override
                            public void run() {


                            }
                        };
                        timer2.scheduleAtFixedRate(t2, 1000, 8000);
                        actualizarEstado(ped.getNumPedidoRef());

                    } else if (estado == 3 || estado == 4) {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        Toast.makeText(context, rptaClientes.getMensaje(), Toast.LENGTH_SHORT).show();
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
//                        Log.e("ADAPTERPEDIDOXREFAC",rptaClientes.getMensaje());
                        Log.e("ADAPTERPEDIDOXREFAC",String.valueOf(response.body()));
                        Toast.makeText(context, "Problemas de conexión , por favor intentelo de nuevo 2", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta<Documento>> call, Throwable t) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    Toast.makeText(context, "Problemas de conexión , por favor intentelo de nuevo.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void actualizarEstado(String nroPedido_ref) {
            //PARA ACTUALIZAR LA TABLA NUEVA LOG_REPARTO PARA HACER JOIN  Y NO PARESCA DE NUEVO EN EL L1ISTADO DE PERDIDOS POR REFACTURAR
            //el pamaetro de paso es el numero de pedido antiguo para actualizar, para que no aparesca en listado por refacturar
            Map<String, String> data = new HashMap<>();
            data.put("placa",sessionusuario.getCodigoAplicacion() );
            data.put("nroPedido",nroPedido_ref);
            Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPedidoService().updateLogRefacturacion(data);
            call.enqueue(new Callback<JsonRespuesta>() {
                @Override
                public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {
                    JsonRespuesta rptaClientes = response.body();
                    Integer estado = rptaClientes.getEstado();

                    if (estado == 1) {

                        Toast.makeText(context, "Pedido Actualizado correctamente.", Toast.LENGTH_LONG).show();
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        listaPedidoAux.remove(getAdapterPosition());
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        notifyDataSetChanged();

                    } else if (estado == 3 || estado == 4) {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        Toast.makeText(context, rptaClientes.getMensaje(), Toast.LENGTH_SHORT).show();
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog2);
                        Toast.makeText(context, "Problemas de conexión , por favor intentelo de nuevo 2", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                    Metodos.hideProgressDialogWithTitle(progressDialog2);
                    Toast.makeText(context, "Problemas de conexión , por favor intentelo de nuevo.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void onClick(View v) {


        }
    }
}
