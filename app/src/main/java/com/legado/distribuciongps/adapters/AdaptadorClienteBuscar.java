package com.legado.distribuciongps.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.InicioFragment;
import com.legado.distribuciongps.fragments.Listado_packing;
import com.legado.distribuciongps.fragments.pedidosXcliente;
import com.legado.distribuciongps.modelo.Cliente;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorClienteBuscar extends RecyclerView.Adapter<AdaptadorClienteBuscar.ViewHolder>{
    private Context context;
    private Activity act;
    private ArrayList<Cliente> listaClienteAux;
    private int posicionItemSeleccionado;


    public AdaptadorClienteBuscar(Context context){
        this.context = context;
        this.listaClienteAux =new ArrayList<>();
    }
    public AdaptadorClienteBuscar(ArrayList<Cliente> items, Context context){
        this.context = context;
        this.listaClienteAux =items;
    }


    public int obtenerPosicionItemSeleccionado(){
        return posicionItemSeleccionado;
    }

    public void updateList(List<Cliente> clientes) {
        listaClienteAux = new ArrayList<>();
        listaClienteAux.addAll(clientes);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_clientes_buscar,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //Mostrar los datos de las rutinas dentro de cada control que contiene el cardView

        //1. Crear un objeto rutina para poder extraer los datos
        Cliente objCliente = listaClienteAux.get(position);

        //2. Mostrar los datos de la packing en los controles del cardView
        holder.txtNombCliente.setText("Cli.: "+objCliente.getNomCliente()+", "+objCliente.getCodCliente());
        holder.txtDireccion.setText("Direcc.: "+objCliente.getDirDespacho());
        holder.txtPackings.setText("Pack.: "+objCliente.getPackings());
        String nombr=objCliente.getNomCliente();
        System.out.println(nombr);
        int op=Integer.parseInt(objCliente.getCantPedidos());
        int op2=Integer.parseInt(objCliente.getTienerebotes());
        String estados=objCliente.getFlagEstadoEntregadoRebotes();
        switch (estados){
            case "1"://TIENE PEDIDOS PENDIENTES
                holder.lblCheckEntregado.setVisibility(View.GONE);
                holder.lblCheckRebotes.setVisibility(View.GONE);
                break;
            case "2"://ENTREGADO
                holder.lblCheckEntregado.setVisibility(View.VISIBLE);
                holder.lblCheckRebotes.setVisibility(View.GONE);
                break;
            case "3":
                holder.lblCheckRebotes.setVisibility(View.VISIBLE);
                holder.lblCheckEntregado.setVisibility(View.GONE);
                break;
        }
//        if (op<=0){
//            if (op2>0){
//                holder.lblCheckRebotes.setVisibility(View.VISIBLE);
//            }else{
//                holder.lblCheckRebotes.setVisibility(View.GONE);
//                holder.lblCheckEntregado.setVisibility(View.VISIBLE);
//            }
//        }else{
//            holder.lblCheckEntregado.setVisibility(View.GONE);
//        }

    }

    @Override
    public int getItemCount() {
        return listaClienteAux.size();
    }

    public void addList(ArrayList<Cliente> arrayList){
        //Permite recivir la lista de biens que esta dentro de la clase bien
        listaClienteAux = arrayList;

        //Permite notificar al Adaptador que cambios en la base de datos
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements  View.OnLongClickListener, View.OnCreateContextMenuListener{
        //Permite declarar y relacionar los controles que estan contenidos dentro del CardView
        TextView txtNombCliente, txtDireccion,txtPackings,lblCheckEntregado,lblCheckRebotes;

        public ViewHolder(View itemView) {
            super(itemView);

            //Relacionar (enlazar) los controles declarados en java con los controles que estan en el CardView
            txtNombCliente = (TextView) itemView.findViewById(R.id.txtNombCliente);
            txtDireccion = (TextView) itemView.findViewById(R.id.txtDireccionCliente);
            txtPackings = (TextView) itemView.findViewById(R.id.txtPackingCliente);
            lblCheckEntregado = (TextView) itemView.findViewById(R.id.lblCheckEntregado);
            lblCheckRebotes = (TextView) itemView.findViewById(R.id.lblCheckRebotes);


//btnEntregar.setOnClickListener(v -> {
//    notifyItemRemoved(obtenerPosicionItemSeleccionado());
//    notifyDataSetChanged();
//});

            itemView.setOnClickListener(this::onClick);
            itemView.setOnLongClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

//        @Override
//        public void onClick(View v) {
//
////            int position = getAdapterPosition();
////            String identificador = listaPackingAux.get(position).getIdentificador_s();
////            String marca = listaPackingAux.get(position).getMarca();
////           Log.e("dio clic",marca);
////
////            Intent obj=new Intent(context,DetalleBienAct.class);
////            obj.putExtra("cod",identificador);
////            context.startActivity(obj);
//            v.
//        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            String nombre = listaPackingAux.get(this.getAdapterPosition()).getNro_packing();
//
//            menu.setHeaderTitle(nombre);
//
//            menu.add(0, 1, 0, "Editar");
//            menu.add(0, 2, 0, "Cancelar");
//            menu.add(0, 3, 0, "Completado");
        }

        @Override
        public boolean onLongClick(View v) {
            posicionItemSeleccionado = this.getAdapterPosition();
            return false;
        }

        private void onClick(View v) {


            int pos = getAdapterPosition();
            String codCliente = listaClienteAux.get(pos).getCodCliente();
            String cliente = listaClienteAux.get(pos).getNomCliente();
            String direc = listaClienteAux.get(pos).getDirDespacho();
            String packing = listaClienteAux.get(pos).getPackings();




            FragmentTransaction frag = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction().addToBackStack("ATRAS");
            Bundle data = new Bundle();
            data.putString("cliente", cliente);
            data.putString("packings", packing);
            data.putString("direc", direc);
            data.putString("codcliente", codCliente);
            data.putString("empresa", Listado_packing.empresa);
            data.putString("almacenes", Listado_packing.almacenes);
//            data.putString("asignaciones", Listado_packing.packings);
            data.putString("asignaciones", packing);
            pedidosXcliente clientesMapa=new pedidosXcliente();
            clientesMapa.setArguments(data);
            frag.replace(R.id.contenedor, clientesMapa);
            frag.commit();




        }
    }
}
