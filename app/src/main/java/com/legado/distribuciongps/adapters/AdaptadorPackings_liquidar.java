package com.legado.distribuciongps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.PorLiquidarFragment;
import com.legado.distribuciongps.modelo.Packing;

import java.util.ArrayList;

public class AdaptadorPackings_liquidar extends RecyclerView.Adapter<AdaptadorPackings_liquidar.ViewHolder>{
    private Context context;
    private ArrayList<Packing> listaPackingAux;
    private int posicionItemSeleccionado;

    public AdaptadorPackings_liquidar(Context context){
        this.context = context;
        this.listaPackingAux = new ArrayList<>();
    }

    public int obtenerPosicionItemSeleccionado(){
        return posicionItemSeleccionado;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_packing_liquidar,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //Mostrar los datos de las rutinas dentro de cada control que contiene el cardView

        //1. Crear un objeto rutina para poder extraer los datos
        Packing packing = listaPackingAux.get(position);

        //2. Mostrar los datos de la packing en los controles del cardView
        holder.txtNroPacking.setText(packing.getNro_packing());
        holder.txtRutas.setText("Rutas: "+packing.getRutas());
        holder.txtFecha.setText("Fecha: "+packing.getFecha());
        holder.txtMonto.setText("Monto Total: "+String.valueOf(packing.getMonto_total()));
//        holder.txtliquidado.setChecked(packing.isFlag_checked());
        if (packing.isFlag_checked()){
            holder.txtliquidado.setVisibility(View.VISIBLE);
        }else{
            holder.txtliquidado.setVisibility(View.GONE);
        }
    }
    @Override
    public int getItemCount() {
        return listaPackingAux.size();
    }

    public void addList(ArrayList<Packing> arrayList){
        //Permite recivir la lista de biens que esta dentro de la clase bien
        listaPackingAux = arrayList;

        //Permite notificar al Adaptador que cambios en la base de datos
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener, View.OnCreateContextMenuListener{
        //Permite declarar y relacionar los controles que estan contenidos dentro del CardView
        TextView txtNroPacking, txtFecha,txtMonto,txtRutas,txtliquidado;
        CheckBox chkSelecionado;
        CardView card_fondo;

        public ViewHolder(View itemView) {
            super(itemView);

            //Relacionar (enlazar) los controles declarados en java con los controles que estan en el CardView
            txtNroPacking = (TextView) itemView.findViewById(R.id.txtNroPacking_item);
            txtFecha = (TextView) itemView.findViewById(R.id.txtFecha_item);
            txtMonto = (TextView) itemView.findViewById(R.id.txtMontoTotal_item);
            txtRutas= (TextView) itemView.findViewById(R.id.txtRutas);
            txtliquidado= (TextView) itemView.findViewById(R.id.txtliquidado);
//            chkSelecionado = (CheckBox) itemView.findViewById(R.id.chkSelecionar_item);
            card_fondo = (CardView) itemView.findViewById(R.id.card_packing_item);
            //card_fondo.setCardBackgroundColor(Color.GRAY);



            itemView.setOnClickListener(v -> {
                Bundle data = new Bundle();
                PorLiquidarFragment clientesMapa = new PorLiquidarFragment();

                data.putString("packings", listaPackingAux.get(this.getAdapterPosition()).getNro_packing());
                data.putString("fecha", listaPackingAux.get(this.getAdapterPosition()).getFecha());

                clientesMapa.setArguments(data);
                FragmentTransaction frag = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().addToBackStack("ATRAS2");

                frag.replace(R.id.content_por_liquidar, clientesMapa);
                frag.commit();
            });
            itemView.setOnLongClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {

//            int position = getAdapterPosition();
//            String identificador = listaPackingAux.get(position).getIdentificador_s();
//            String marca = listaPackingAux.get(position).getMarca();
//           Log.e("dio clic",marca);
//
//            Intent obj=new Intent(context,DetalleBienAct.class);
//            obj.putExtra("cod",identificador);
//            context.startActivity(obj);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            String nombre = listaPackingAux.get(this.getAdapterPosition()).getNro_packing();
//
//            menu.setHeaderTitle(nombre);
//
//            menu.add(0, 1, 0, "Editar");
//            menu.add(0, 2, 0, "Cancelar");
//            menu.add(0, 3, 0, "Completado");
        }

        @Override
        public boolean onLongClick(View v) {
            posicionItemSeleccionado = this.getAdapterPosition();
            return false;
        }
    }
}
