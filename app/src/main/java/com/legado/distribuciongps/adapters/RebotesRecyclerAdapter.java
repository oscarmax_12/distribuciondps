package com.legado.distribuciongps.adapters;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.AdapterCommunication;
import com.legado.distribuciongps.util.SessionUsuario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by __Adrian__ on 19/4/2017.
 */

public class RebotesRecyclerAdapter extends RecyclerView.Adapter<RebotesRecyclerAdapter.ReboteRecyclerViewHolder> implements ItemReboteListener {
    private static final String TAG = "RebotesRecyclerAdapter";

    private List<Rebote> documentoDeudas = new ArrayList<>();
    private FragmentActivity activity;
    private AdapterCommunication mListener;
    private Context context;
    private String codCliente;
    private SessionUsuario sessionUsuario;
    Fragment fragment;
    // MyDialogProgress dialogPago;

    SwipeRefreshLayout swiperefresh;
    Bundle args;

    public void setOnClickListener(AdapterCommunication listener) {
        mListener = listener;
    }


    public RebotesRecyclerAdapter(List<Rebote> documentoDeudas, FragmentActivity activity, SwipeRefreshLayout swiperefresh, Fragment fragment) {
        this.activity = activity;
        this.documentoDeudas = documentoDeudas;
        this.swiperefresh = swiperefresh;
        this.fragment = fragment;
    }

    @Override
    public ReboteRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mis_rebotes, parent, false);
        sessionUsuario = new SessionUsuario(v.getContext());
        return new ReboteRecyclerViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(final ReboteRecyclerViewHolder holder, final int position) {
        holder.numeroDoc.setText(documentoDeudas.get(position).getSerie() + "-" + documentoDeudas.get(position).getPreimpreso());
        holder.tipoDoc.setText(documentoDeudas.get(position).getTipoDoc());
//   holder.codCliente.setText(documentoDeudas.get(position).getCodCliente());
        holder.descCliente.setText(documentoDeudas.get(position).getTipoRebote());
        holder.monto.setText(documentoDeudas.get(position).getMonto().toString());

        holder.itemView.setSelected(true);
//        holder.itemView.setBackgroundResource(R.drawable.selector_row_cobranza);


    }


    private void showLoginError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public int getItemCount() {
        return documentoDeudas.size();
    }

    public BigDecimal montoTotalRebote() {
        BigDecimal montoTotal = new BigDecimal(0);
        for (int i = 0; i < documentoDeudas.size(); i++) {
            montoTotal = montoTotal.add(documentoDeudas.get(i).getMonto());
        }
        return montoTotal;
    }


    public List<Rebote> listaRebotes() {
        return documentoDeudas;
    }

    public void updateList(List<Rebote> planilla) {
        documentoDeudas = new ArrayList<>();
        documentoDeudas.addAll(planilla);
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {

//        CobranzaFragment cobranzaFragment=new CobranzaFragment();
//        cobranzaFragment.setArguments(args);
//        Bundle args = new Bundle();
//        args.putString("codCliente", documentoDeudas.get(position).getCodCliente());
//        args.putBoolean("fromPlanilla", true);
//        cobranzaFragment.setArguments(args);
//        FragmentTransaction ft =  activity.getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.content_planilla_cobranza, cobranzaFragment,TAG);
//        ft.addToBackStack("PlanillaCobranzaFragment");
//        ft.commit();
    }


    public static class ReboteRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView numeroDoc;
        public TextView codCliente;
        public TextView descCliente;
        public TextView tipoDoc;
        public TextView monto;
        public TextView saldo;
        public TextView txtEstado;
        public TextView txtCodVendedor;
        public TextView txtDireccion;
        public ItemReboteListener listener;

        public ReboteRecyclerViewHolder(View itemView, ItemReboteListener listener) {
            super(itemView);
            numeroDoc = (TextView) itemView.findViewById(R.id.txtNumeroDoc);

            monto = (TextView) itemView.findViewById(R.id.txtMonto);

            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(view, getAdapterPosition());
        }
    }
}

interface ItemReboteListener {

    void onItemClick(View view, int position);
}



