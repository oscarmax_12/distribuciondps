package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.toFrameMapa;
import com.legado.distribuciongps.adapters.AdaptadorPackings_liquidar;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Packing;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class Listado_packing_liquidar extends Fragment {

    AdaptadorPackings_liquidar adapterPackings;
    RecyclerView recyclerPackings;
    Button btnSeleccionarPacking;
    TextView txt_lista_vacia;
    SessionUsuario session;
    ProgressDialog progressDialog;

    public static String packings = "", almacenes = "", empresa = "", placa;

    public Listado_packing_liquidar() {
        // Required empty public constructor
    }
    public static Listado_packing_liquidar newInstance() {
        Listado_packing_liquidar fragment = new Listado_packing_liquidar();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_listado_packing_liquidar, container, false);
        recyclerPackings = (RecyclerView) view.findViewById(R.id.recyclerPackings_liquidar);
        txt_lista_vacia = (TextView) view.findViewById(R.id.txt_lista_vacia_liquidar);
        recyclerPackings.setHasFixedSize(true);
        recyclerPackings.setLayoutManager(new LinearLayoutManager(this.getContext()));
        session = new SessionUsuario(getContext());
        Metodos.validarSesion(getActivity());
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(getContext(), "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            getActivity().finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(getContext(), "ERROR!", "Error");
            getActivity().finish();
        }
//        placa = getArguments().getString("placa");
        progressDialog=new ProgressDialog(getContext());
        Metodos.showProgressDialogWithTitle("Cargando listado de packings", progressDialog);
        cargarPackings(session.getCodigoAplicacion());
//        ((toFrameMapa) getActivity()).hideFloatingActionButton();


        return view;
    }
    public void cargarPackings(String placa) {
        Map<String, String> data = new HashMap<>();
        data.put("placa", placa);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getPackingService().ListarPackingXPlaca(data);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();

                    int estado = response.body().getEstado();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        Packing.listaPackings.clear();
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Packing objPacking = new Packing();
                                objPacking.setNro_packing(objItem.getString("nro_Packings"));
                                objPacking.setFecha( objItem.getString("fecha").trim());
                                objPacking.setMonto_total(objItem.getDouble("montoTotal"));
                                objPacking.setCodAlmacen(objItem.getString("codAlmacenes"));
                                objPacking.setCodEmpresa(objItem.getString("codEmpresa"));
                                objPacking.setRutas(objItem.getString("rutas"));
                                objPacking.setIsHoy(objItem.getString("ishoy"));
//                                objPacking.setIsHoy(objItem.getString("ishoy"));
                                int op=Integer.parseInt(objItem.getString("liquidado"));
                                if(op>0){
                                    objPacking.setFlag_checked(true);
                                }else{
                                    objPacking.setFlag_checked(false);
                                }

//                    Log.e("BIEN", jsonDatosRutina.getString("identificador_sin"));
                                Log.e("PACKINGS",objItem.getString("nro_Packings"));
                                Packing.listaPackings.add(objPacking);
                            }

                            adapterPackings = new AdaptadorPackings_liquidar(getContext());
                            adapterPackings.addList(Packing.listaPackings);
                            recyclerPackings.setAdapter(adapterPackings);
                            recyclerPackings.setVisibility(View.VISIBLE);
                            txt_lista_vacia.setVisibility(View.GONE);
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                        } else {
                            Metodos.hideProgressDialogWithTitle(progressDialog);
                            recyclerPackings.setVisibility(View.GONE);
                            txt_lista_vacia.setVisibility(View.VISIBLE);
                            MensajesDialogAlert.mensajeInformacion(getContext(), "Información", "No se encontraron locales");

                        }
                    } else {
                        Metodos.hideProgressDialogWithTitle(progressDialog);
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    Metodos.hideProgressDialogWithTitle(progressDialog);
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR ", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }
}
