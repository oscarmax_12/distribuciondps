package com.legado.distribuciongps.fragments;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Placa;
import com.legado.distribuciongps.util.ListPlacaAdapter;
import com.legado.distribuciongps.util.SessionUsuario;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacasFragment extends DialogFragment {

    @BindView(R.id.listPlacas)
    ListView listPlacas;
    private List<Placa> placas = new ArrayList<Placa>();
    private ListPlacaAdapter adapter;
    private SessionUsuario sessionUsuario;
    private GenerarPedidoFragment fragment;

    public static PlacasFragment newInstance(Bundle arguments, List<Placa> placas,GenerarPedidoFragment fragment){
        PlacasFragment f = new PlacasFragment();
        if(arguments != null){
            f.setPlacas(placas);
            f.setFragment(fragment);
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_placas, container, false);
        ButterKnife.bind(this,rootView);
        getDialog().setTitle("Lista de Placas");
       // setStyle(DialogFragment.STYLE_NORMAL,R.style.MyDialogFragmentStyle);
        setStyle (DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Dialog_NoActionBar);
        setCancelable(true);
        getDialog().setCanceledOnTouchOutside(false);
        adapter = new ListPlacaAdapter(getContext(), placas, getActivity());
        listPlacas.setAdapter(adapter);
        sessionUsuario = new SessionUsuario(getActivity());
        listPlacas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String placa = String.valueOf(adapter.getPlacas().get(position).getNumPlaca());
                sessionUsuario.CargarCodigo(placa);
                fragment.RecargarPlaca();
                getDialog().dismiss();
            }
        });
        return rootView;
    }

    public List<Placa> getPlacas() {
        return placas;
    }

    public void setPlacas(List<Placa> placas) {
        this.placas = placas;
    }

    public GenerarPedidoFragment getFragment() {
        return fragment;
    }

    public void setFragment(GenerarPedidoFragment fragment) {
        this.fragment = fragment;
    }
}
