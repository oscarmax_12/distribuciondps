package com.legado.distribuciongps.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.toFrameMapa;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Cliente;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.PaqueteClientes;
import com.legado.distribuciongps.util.SessionUsuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class RutasFrag extends Fragment implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback {
    private static RutasFrag instance = null;
    View login_progress;
    FrameLayout frm;
    GoogleMap map;
    static ArrayList<Cliente> listaClienteSinCoordenadas = new ArrayList<>();
    ProgressDialog progressDialog;

    private SupportMapFragment mapFragment;
    private LocationRequest mLocationRequest;
    private Location location;
    HashMap<String, String> markerMap = new HashMap<String, String>();

    double x = 0;
    double y = 0;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private BitmapDescriptor iconUser, iconClient;
    String empresa = "", almacenes = "", asignaciones = "";
    private SessionUsuario sessionUsuario;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private Location mLastKnownLocation;
    public static SharedPreferences sharedPref;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    FloatingActionButton fabRealoadMap;

    public RutasFrag() {
        // Required empty public constructor
    }

    public static RutasFrag getInstance() {
        if (instance == null) {
            Log.e("ingreso", "getinstance");
            instance = new RutasFrag();
        }
        return instance;
    }

    public void crearInfo() {
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View myContentView = getLayoutInflater().inflate(
                        R.layout.custom_infowindow, null);

                TextView tvTitle = ((TextView) myContentView.findViewById(R.id.txtPedidoInfo));
                tvTitle.setText(marker.getTitle());
                TextView tvSnippet = ((TextView) myContentView.findViewById(R.id.txtMontoInfo));
                tvSnippet.setText(marker.getSnippet());
                return myContentView;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rutas, container, false);
        sessionUsuario = new SessionUsuario(getActivity());
        login_progress = (View) view.findViewById(R.id.login_progress_map);
        progressDialog = new ProgressDialog(getContext());
        fabRealoadMap = (FloatingActionButton) view.findViewById(R.id.fabReloadMap);


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
//        mapFragment.getMapAsync(this);
        ((toFrameMapa) getActivity()).showFloatingActionButton();
        sharedPref = getContext().getSharedPreferences("shared", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
//        frm = (FrameLayout) view.findViewById(R.id.map);
//        String empresa="", almacenes="",asignaciones="";

        if (getArguments() != null) {
            empresa = getArguments().getString("empresa");
            almacenes = getArguments().getString("almacenes");
            asignaciones = getArguments().getString("asignaciones");
//            editor.putString("recarga", "si");
//            editor.commit();
        }

        String recarga = sharedPref.getString("recarga", "");
        String depaso = sharedPref.getString("depaso", "");
        fabRealoadMap.setOnClickListener(v -> {
//            if (recarga.equalsIgnoreCase("si")&&depaso.equalsIgnoreCase("no")) {
            String empresa = sharedPref.getString("empresa", "");
            String almacenes = sharedPref.getString("almacenes", "");
            String asignaciones = sharedPref.getString("asignaciones", "");
            cargarCoordenadas(empresa, almacenes, asignaciones);

        });
        if (recarga.equalsIgnoreCase("no")) {

            editor.remove("empresa");
            editor.remove("almacenes");
            editor.remove("asignaciones");
            editor.putString("empresa", empresa);
            editor.putString("almacenes", almacenes);
            editor.putString("asignaciones", asignaciones);
            editor.putString("recarga", "si");
            editor.putString("depaso", "no");
            editor.commit();

            if (empresa.equals("")) {
                MensajesDialogAlert.mensajeError(getContext(), "Parametro error", empresa);
                System.exit(1);
            }
//        Log.e("PARAMETROS FRAGMENT:",placa+" y "+fecha);


            try {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    MensajesDialogAlert.mensajeError(getContext(), "Error", "No se dio permiso para coordenadas");

                } else {
                    cargarCoordenadas(empresa, almacenes, asignaciones);
                }
            } catch (Exception e) {
                e.printStackTrace();
                getActivity().finish();
            }
        } else {
//            String carga = sharedPref.getString("recarga", "");
            if (recarga.equalsIgnoreCase("si") && depaso.equalsIgnoreCase("no")) {
                String empresa = sharedPref.getString("empresa", "");
                String almacenes = sharedPref.getString("almacenes", "");
                String asignaciones = sharedPref.getString("asignaciones", "");
                cargarCoordenadas(empresa, almacenes, asignaciones);
            } else {

            }
        }


//        cargarCoordenadas(empresa,almacenes,asignaciones);
//        setUpMapIfNeeded();

//        map.setOnInfoWindowClickListener(this);


        return view;
    }

    public boolean isPreferencesSet(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Log.e("bandera preferences", String.valueOf(sharedPref.getAll().size() > 0));
        Log.e("cuantos hay", String.valueOf(sharedPref.getAll().size()));
        return (sharedPref.getAll().size() > 1);
    }

    // Method to show Progress bar
    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    // Method to hide/ dismiss Progress bar
    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void cargarCoordenadas(String empresa, String almacenes, String asignaciones) {
        Map<String, String> data = new HashMap<>();
        showProgressDialogWithTitle("CARGANDO CLIENTES CON PACKINGS SELECIONADOS");
        data.put("empresa", empresa);
        data.put("almacenes", almacenes);
        data.put("asignaciones", asignaciones);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getClienteService().ListarClientesXPackings(data);
        Log.e("Hizo la peticion", call.toString());
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, retrofit2.Response<JsonRespuesta> response) {

                try {
                    ArrayList array = (ArrayList) response.body().getData();

                    int estado = response.body().getEstado();
                    if (estado == 200) {
                        JSONArray jsonArray = new JSONArray(array);
                        if (jsonArray.length() > 0) {

                            map.clear();
                            listaClienteSinCoordenadas.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objItem = jsonArray.getJSONObject(i);
                                Cliente objCliente = new Cliente();

                                Log.e("OBJECT JSON", objItem.toString());
                                if (!(objItem.getString("x").isEmpty() || objItem.getString("x").equals(null) || objItem.getString("x").contains("null"))) {
                                    RutasFrag.this.x = Double.parseDouble(objItem.getString("x"));
                                    RutasFrag.this.y = Double.parseDouble(objItem.getString("y"));
                                    String title = objItem.getString("nomCliente") + "|" + objItem.getString("codCliente");
                                    String asig = objItem.getString("nro_asig_transporte");
                                    String direc = objItem.getString("dirDespacho");
                                    int cantPedidos = Integer.parseInt(objItem.getString("cantPedidos"));
                                    int cantrebotes = Integer.parseInt(objItem.getString("tienerebotetotal"));
                                    UiSettings ui = map.getUiSettings();
                                    ui.setZoomControlsEnabled(true);

                                    if (cantPedidos > 0) {
                                        objCliente.setFlagEstadoEntregadoRebotes("1");
                                        if (objItem.getString("flagCreditoContado").equals("0")) {
                                            map.addMarker(new MarkerOptions().
                                                    position(new LatLng(x, y)).title(title).snippet("Pak|" + asig + "|" + direc).
                                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                                        } else {

                                            map.addMarker(new MarkerOptions().
                                                    position(new LatLng(x, y)).title(title).snippet("Pak|" + asig + "|" + direc).
                                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                                        }
                                    } else {
                                        if (cantrebotes > 0) {
                                            objCliente.setFlagEstadoEntregadoRebotes("3");
                                            map.addMarker(new MarkerOptions().
                                                    position(new LatLng(x, y)).title(title).snippet("Pak|" + asig + "|" + direc).
                                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                                        } else {
                                            objCliente.setFlagEstadoEntregadoRebotes("2");
                                            map.addMarker(new MarkerOptions().
                                                    position(new LatLng(x, y)).title(title).snippet("Pak|" + asig + "|" + direc).
                                                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                        }


                                    }


                                    objCliente.setNomCliente(objItem.getString("nomCliente"));
                                    objCliente.setCodCliente(objItem.getString("codCliente"));
                                    objCliente.setDirDespacho(objItem.getString("dirDespacho"));
                                    objCliente.setPackings(objItem.getString("nro_asig_transporte"));
                                    objCliente.setCantPedidos(objItem.getString("cantPedidos"));
                                    objCliente.setTienerebotes(objItem.getString("tienerebotetotal"));
                                    listaClienteSinCoordenadas.add(objCliente);
                                } else {
                                    int cantPedidos = Integer.parseInt(objItem.getString("cantPedidos"));
                                    int cantrebotes = Integer.parseInt(objItem.getString("tienerebotetotal"));
                                    if (cantPedidos > 0) {
                                        objCliente.setFlagEstadoEntregadoRebotes("1");
                                    } else {
                                        if (cantrebotes > 0) {
                                            objCliente.setFlagEstadoEntregadoRebotes("3");
                                        } else {
                                            objCliente.setFlagEstadoEntregadoRebotes("2");
                                        }
                                    }
                                    objCliente.setNomCliente(objItem.getString("nomCliente"));
                                    objCliente.setCodCliente(objItem.getString("codCliente"));
                                    objCliente.setDirDespacho(objItem.getString("dirDespacho"));
                                    objCliente.setPackings(objItem.getString("nro_asig_transporte"));
                                    objCliente.setCantPedidos(objItem.getString("cantPedidos"));
                                    objCliente.setTienerebotes(objItem.getString("tienerebotetotal"));
                                    listaClienteSinCoordenadas.add(objCliente);
                                    Log.e("clienteSin", objItem.toString());
                                }
                                PaqueteClientes paqueteClientes = new PaqueteClientes();
                                paqueteClientes.setListaClientes(listaClienteSinCoordenadas);
                                sessionUsuario.guardarPaqueteCliente(null);
                                sessionUsuario.guardarPaqueteCliente(paqueteClientes);
                            }
                            CameraPosition posicionCamara = new CameraPosition.Builder()
                                    .target(getCenterCoordinate())
                                    .zoom(15)
                                    .build();
                            CameraUpdate camara = CameraUpdateFactory.newCameraPosition(posicionCamara);
                            map.animateCamera(camara);
                            map.setMyLocationEnabled(true);
                            hideProgressDialogWithTitle();
                            map.setOnInfoWindowClickListener(marker -> {
                                String title = marker.getTitle();
                                String snipet = marker.getSnippet();
                                FragmentTransaction frag = getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("CLIENTE-MAPA");
                                Bundle data1 = new Bundle();
                                String[] parts = title.split("\\|");
                                String[] partsPacking = snipet.split("\\|");
//                                    String cod = title.substring(title.indexOf("\\|") + 1);
                                data1.putString("cliente", parts[0]);
                                data1.putString("packings", partsPacking[1]);
                                data1.putString("direc", partsPacking[2]);
                                data1.putString("codcliente", parts[1]);
                                data1.putString("empresa", empresa);
                                data1.putString("almacenes", almacenes);
                                data1.putString("asignaciones", asignaciones);
                                pedidosXcliente clientesMapa = new pedidosXcliente();
                                clientesMapa.setArguments(data1);
                                SharedPreferences.Editor editor = RutasFrag.sharedPref.edit();
                                editor.putString("depaso", "si");
                                editor.commit();
                                frag.replace(R.id.contenedor, clientesMapa);
                                frag.commit();
                            });

                        } else {
                            hideProgressDialogWithTitle();
                            MensajesDialogAlert.mensajeInformacion(getContext(), "Información", "No se encontraron locales");

                        }
                    } else {
                        hideProgressDialogWithTitle();
                        MensajesDialogAlert.mensajeError(getContext(), "Error", "No se encontraron locales");
                    }
                } catch (Exception e) {
                    hideProgressDialogWithTitle();
                    MensajesDialogAlert.mensajeError(getContext(), "Error", e.getMessage());
                    e.printStackTrace();
                    Log.e("Error-r", "Error de funcionamiento, contactar con soporte GPS");
                }


                //progressDialog.dismiss();
//                showProgress(false);

            }

            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {
                Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR RUTAS", Toast.LENGTH_LONG).show();
                //progressDialog.dismiss();
//            showProgress(false);
            }
        });
    }

    public void permisoGPS() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), 15));
                        } else {
//                            Log.d("SI ENTRO", "Current location is null. Using defaults.");
//                            Log.e("EMTRO", "Exception: %s", task.getException());
//                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, 15));
//                            map.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    public void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map != null) {
            // Try to obtain the map from the SupportMapFragment.
//            map = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            map.setMyLocationEnabled(true);
            // Check if we were successful in obtaining the map.
            if (map != null) {


                map.setOnMyLocationChangeListener(arg0 -> {
                    // TODO Auto-generated method stub
                    Toast.makeText(getContext(), "locacion" + arg0.getLatitude(), Toast.LENGTH_LONG).show();
                    Toast.makeText(getContext(), "locacion" + arg0.getLongitude(), Toast.LENGTH_LONG).show();
//                        map.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).title("It's Me!"));
                });

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
//permisoGPS();

        map = googleMap;
        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
//        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        //getDeviceLocation();

//        map.setMyLocationEnabled(true);


    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e("TAG", "SUCCESS");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("TAG", "RESOLUTION_REQUIRED");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e("TAG", "GPS NO DISPONIBLE");
                        break;
                }
            }
        });
    }


    //    PARA MAPA==================
    public LatLng getCenterCoordinate() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(this.x, this.y));
        LatLngBounds bounds = builder.build();
        return bounds.getCenter();
    }


//    @Override
//    public void onInfoWindowClick(Marker marker) {
//        String actionId = markerMap.get(marker.getId());
//        pedidosXcliente clientesMapa=new pedidosXcliente();
////        data.putString("empresa", empresa);
////        data.putString("almacenes", almacenes);
////        data.putString("asignaciones", packings);
////        clientesMapa.setArguments(data);
//        FragmentTransaction frag = getActivity().getSupportFragmentManager().beginTransaction();
//        frag.replace(R.id.contenedor, clientesMapa);
//        frag.commit();
//    }
}
