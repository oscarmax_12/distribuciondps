package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.adapters.EntregasRecyclerAdapter;
import com.legado.distribuciongps.adapters.RebotesRecyclerAdapter;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisEntregasFragment extends Fragment {


    @BindView(R.id.reciclador)
    RecyclerView reciclador;

    private RecyclerView.LayoutManager lManager;
    @BindView(R.id.txtTotalContados)
    TextView txtTotalContados;

    //    @BindView(R.id.swiperefresh)
//    SwipeRefreshLayout swiperefresh;
    ProgressDialog progressDialog;
    private SessionUsuario sessionUsuario;
    EntregasRecyclerAdapter entregasRecyclerAdapter;

    public static MisEntregasFragment newInstance() {
        MisEntregasFragment fragment = new MisEntregasFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mis_entregas, container, false);
        ButterKnife.bind(this, rootView);
        sessionUsuario = new SessionUsuario(getContext());
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        Metodos.validarSesion(getActivity());
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(getContext(), "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            getActivity().finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(getContext(), "ERROR!", "Error");
            getActivity().finish();
        }
        cargarEntregasByPlaca(sessionUsuario.getCodigoAplicacion());
        // setHasOptionsMenu(true);
//        swiperefresh.setOnRefreshListener(
//                new SwipeRefreshLayout.OnRefreshListener() {
//                    @Override
//                    public void onRefresh() {
//                       // cargarRebotesByPlaca(sessionUsuario.getCodigoAplicacion());
//                        swiperefresh.setRefreshing(false);
//                    }
//                });


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inicio, menu);

    }

    public void cargarEntregasByPlaca(String placa) {
        progressDialog.show();
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("placa", placa);
        dataConsulta.put("codSede", sessionUsuario.getCodSede());
        Call<JsonRespuesta<Rebote>> call = ApiRetrofit.getInstance().getMetricasService().getEntregasByPlaca(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta<Rebote>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<Rebote>> call, Response<JsonRespuesta<Rebote>> response) {
                if (response.code() == 401) {
                    Toast.makeText(
                            getActivity(), "SE EXPIRÓ EL TIEMPO DE LA TOMA DE PEDIDOS PARA SU USUARIO ,POR FAVOR COMUNIQUESE CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //  progressDialog.dismiss();
                } else if (response.code() == 403) {
                    Toast.makeText(getActivity(), "USUARIO INACTIVO, COMUNIQUESE CON CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //   progressDialog.dismiss();

                } else {
                    reciclador.setVisibility(View.VISIBLE);
                    //txtEmpty.setVisibility(View.GONE);
                    reciclador.setHasFixedSize(true);
                    lManager = new LinearLayoutManager(getActivity());
                    reciclador.setLayoutManager(lManager);
                    JsonRespuesta<Rebote> rpta = response.body();

                    if (rpta.getData().isEmpty()) {
                        MensajesDialogAlert.mensajeInformacion(getContext(), "info", "NO PRESENTA PAGOS A CONTADO");

                    } else {
                        entregasRecyclerAdapter = new EntregasRecyclerAdapter(rpta.getData(), getActivity(), MisEntregasFragment.this);
                        txtTotalContados.setText(entregasRecyclerAdapter.montoTotalLiquidacion().toString());
                        reciclador.setAdapter(entregasRecyclerAdapter);
                    }


                    progressDialog.dismiss();
//
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<Rebote>> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(getView(), "Problemas de conexión  ", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Reintentar", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // cargarPlanillaCobranza();

                            }
                        }).show();
                //
            }
        });
    }


}
