package com.legado.distribuciongps.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.FacturarActivity;
import com.legado.distribuciongps.activities.PedidoActivity;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Asignacion;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.TipoDocumento;
import com.legado.distribuciongps.util.AsignacionAdapter;
import com.legado.distribuciongps.util.SessionUsuario;
import com.legado.distribuciongps.util.TipoDocumentoAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ERICK on 06/06/2018.
 */

public class GenerarPedidoFragment extends Fragment {

    @BindView(R.id.txtTextoVersion)
    TextView txtTextoVersion;
    public static final String TAG = GenerarPedidoFragment.class.getSimpleName();
    SessionUsuario sessionUsuario;
    private List<Asignacion> accesos = null;
    @BindView(R.id.spinnerEmpresas)
    public Spinner spinnerEmpresas;
    @BindView(R.id.spinnerTipoDoc)
    Spinner spinnerTipoDoc;
    @BindView(R.id.btnEditarPedido)
    Button btnEditarPedido;
    @BindView(R.id.btnNuevoPedido)
    Button btnNuevoPedido;
    @BindView(R.id.txtnumpedido)
    TextView txtnumpedido;
    @BindView(R.id.txtPlaca)
    TextView txtPlaca;
    public ProgressDialog progressDialog;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("CONSULTAR PEDIDOS");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_generarpedido, container, false);
        ButterKnife.bind(this,rootView);
        progressDialog = new ProgressDialog(getActivity(),R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        sessionUsuario = new SessionUsuario(getActivity());
        txtTextoVersion.setText("App Facturación V 1.2");
        txtPlaca.setText("Placa: "+sessionUsuario.getCodigoAplicacion());
        List<TipoDocumento> tipos = new ArrayList<TipoDocumento>();
        tipos.add(new TipoDocumento("01","FACTURA"));
        tipos.add(new TipoDocumento("02","BOLETA DE VENTA"));
        accesos = sessionUsuario.getAsignaciones();
        loadSpinnerEmpresaSede();
        loadSpinnerTipoDoc(tipos);
        btnEditarPedido.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Asignacion asignacion = (Asignacion)spinnerEmpresas.getSelectedItem();
                TipoDocumento tipoDocumento = (TipoDocumento)spinnerTipoDoc.getSelectedItem();
                progressDialog.show();
                Map<String, String> dataClientes = new HashMap<>();
                dataClientes.put("codemp", asignacion.getEmpresa() != null ? asignacion.getEmpresa().getCodEmpresa().toUpperCase() : "00");
                dataClientes.put("codlocal", asignacion != null ? asignacion.getLocalidad().toUpperCase() : "00");
                dataClientes.put("codtipdoc", tipoDocumento != null ? tipoDocumento.getCodigo().toUpperCase() : "00");
                dataClientes.put("numpedido", txtnumpedido.getText().toString());
                dataClientes.put("numplaca",sessionUsuario.getCodigoAplicacion());
                Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getPedidoService().ObtenerPedido(dataClientes);
                call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
                    @Override
                    public void onResponse(Call<JsonRespuesta<Pedido>> call, retrofit2.Response<JsonRespuesta<Pedido>> response) {
                        JsonRespuesta<Pedido> respuesta = response.body();
                        Pedido pedido = respuesta.getItem();
                        Integer estado = respuesta.getEstado();
                        if (estado == 1) {
                            Intent intent = new Intent(getActivity(),PedidoActivity.class);
                            Gson gson = new Gson();
                            pedido.getVendedor().setUsuario(sessionUsuario.getNickUsuario());
                            pedido.setNumPedidoRef(txtnumpedido.getText().toString());
                            String pedidoJson = gson.toJson(pedido);
                            Log.e("PedidoJson",pedidoJson);
                            intent.putExtra("pedido",pedidoJson);
                            startActivity(intent);
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
                        Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });

        btnNuevoPedido.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Asignacion asignacion = (Asignacion) spinnerEmpresas.getSelectedItem();
                TipoDocumento tipoDocumento = (TipoDocumento) spinnerTipoDoc.getSelectedItem();
                progressDialog.show();
                Map<String, String> dataClientes = new HashMap<>();
                dataClientes.put("codemp", asignacion.getEmpresa() != null ? asignacion.getEmpresa().getCodEmpresa().toUpperCase() : "00");
                dataClientes.put("codlocal", asignacion != null ? asignacion.getLocalidad().toUpperCase() : "00");
                dataClientes.put("codtipdoc", tipoDocumento != null ? tipoDocumento.getCodigo().toUpperCase() : "00");
                dataClientes.put("numpedido", txtnumpedido.getText().toString());
                Call<JsonRespuesta<Pedido>> call = ApiRetrofit.getInstance().getPedidoService().ObtenerNuevoPedido(dataClientes);
                call.enqueue(new Callback<JsonRespuesta<Pedido>>() {
                    @Override
                    public void onResponse(Call<JsonRespuesta<Pedido>> call, retrofit2.Response<JsonRespuesta<Pedido>> response) {
                        JsonRespuesta<Pedido> respuesta = response.body();
                        Pedido pedido = respuesta.getItem();
                        Integer estado = respuesta.getEstado();

                        if (estado == 1) {
                            Intent intent = new Intent(getActivity(), FacturarActivity.class);
                            Gson gson = new Gson();
                            pedido.setEmpresa(asignacion.getEmpresa());
                            pedido.setNumPedidoRef(txtnumpedido.getText().toString());
                            String pedidoJson = gson.toJson(pedido);
                            intent.putExtra("pedido", pedidoJson);
                            startActivity(intent);
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(getContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonRespuesta<Pedido>> call, Throwable t) {
                        Toast.makeText(getContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });
        return rootView;
    }

    private void loadSpinnerEmpresaSede() {
        AsignacionAdapter vendedorSpinnerAdapter = new AsignacionAdapter(getActivity(), R.id.descEmpresa, accesos);
        vendedorSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmpresas.setAdapter(vendedorSpinnerAdapter);
    }

    private void loadSpinnerTipoDoc(List<TipoDocumento> tipos) {
        TipoDocumentoAdapter tipoSpinnerAdapter = new TipoDocumentoAdapter(getActivity(), R.id.descEmpresa, tipos);
        tipoSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoDoc.setAdapter(tipoSpinnerAdapter);
    }

    public Spinner getSpinnerEmpresas() {
        return spinnerEmpresas;
    }

    public void setSpinnerEmpresas(Spinner spinnerEmpresas) {
        this.spinnerEmpresas = spinnerEmpresas;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public void RecargarPlaca(){
        txtPlaca.setText("Placa: "+sessionUsuario.getCodigoAplicacion());
    }

    public void RecargarUrl(String url){
        System.out.println("OLA");
        System.out.println(url);
        ApiRetrofit.BorrarInstance();
        ApiRetrofit.getInstance(url);
    }


}