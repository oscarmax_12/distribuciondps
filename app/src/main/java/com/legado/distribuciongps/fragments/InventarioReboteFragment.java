package com.legado.distribuciongps.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.adapters.ArticuloReboteAdapter;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.ArticuloRebote;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.SessionUsuario;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InventarioReboteFragment extends Fragment {



    @BindView(R.id.reciclador)
    RecyclerView reciclador;
    @BindView(R.id.list_empty)
    TextView txtEmpty;
    private RecyclerView.LayoutManager lManager;


    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    ProgressDialog progressDialog;
    private SessionUsuario sessionUsuario;
    ArticuloReboteAdapter articuloReboteAdapter;

    public static InventarioReboteFragment newInstance() {
        InventarioReboteFragment fragment = new InventarioReboteFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inventario_rebotes, container, false);
        ButterKnife.bind(this, rootView);
        sessionUsuario=new SessionUsuario(getContext());
        progressDialog = new ProgressDialog(getActivity(),R.style.AppTheme_MyDialog);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        // cargarInventarioOffline();

        cargarInventarioReboteByPlaca();
        // setHasOptionsMenu(true);
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        cargarInventarioReboteByPlaca();
                        swiperefresh.setRefreshing(false);
                    }
                });




        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inicio, menu);

    }

    public void cargarInventarioReboteByPlaca(){
        progressDialog.show();
        Map<String, String> dataConsulta = new HashMap<>();
        dataConsulta.put("placa",sessionUsuario.getCodigoAplicacion());
        dataConsulta.put("codSede",sessionUsuario.getCodSede());
        Call<JsonRespuesta<ArticuloRebote>> call =   ApiRetrofit.getInstance().getMetricasService().getInventarioReboteByPlaca(dataConsulta);
        call.enqueue(new Callback<JsonRespuesta<ArticuloRebote>>() {
            @Override
            public void onResponse(Call<JsonRespuesta<ArticuloRebote>> call, Response<JsonRespuesta<ArticuloRebote>> response) {
                if(response.code()==401){
                    Toast.makeText(
                            getActivity(), "SE EXPIRÓ EL TIEMPO DE LA TOMA DE PEDIDOS PARA SU USUARIO ,POR FAVOR COMUNIQUESE CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //  progressDialog.dismiss();
                }else if(response.code()==403){
                    Toast.makeText(getActivity(), "USUARIO INACTIVO, COMUNIQUESE CON CON SU COORDINADOR.",
                            Toast.LENGTH_LONG).show();
                    //   progressDialog.dismiss();

                }else {
                    reciclador.setVisibility(View.VISIBLE);
                    txtEmpty.setVisibility(View.GONE);
                    reciclador.setHasFixedSize(true);
                    lManager = new LinearLayoutManager(getActivity());
                    reciclador.setLayoutManager(lManager);
                    JsonRespuesta<ArticuloRebote> rpta = response.body();
                    List<ArticuloRebote> listaSeccionBonif=new ArrayList<>();
                    List<ArticuloRebote> listaSeccionNormal=new ArrayList<>();
                    List<ArticuloRebote> listaSeccionRef=new ArrayList<>();
                    List<List<ArticuloRebote>> listaAdapter=new ArrayList<>();
                    for (int i = 0; i < rpta.getData().size(); i++) {
                        ArticuloRebote ar=rpta.getData().get(i);
                        if(ar.getTipoRebote().equals("M")){
                            ar.setTipoRebote("MERCADERIA");
                            listaSeccionBonif.add(ar);
                        }else if(ar.getTipoRebote().equals("B")){
                            ar.setTipoRebote("BONIFICACIONES");
                            listaSeccionNormal.add(ar);
                        }else if(ar.getTipoRebote().equals("R")){
                            ar.setTipoRebote("REFACTURADOS");
                            listaSeccionRef.add(ar);
                        }
                    }

                    if(rpta.getData().isEmpty()){
                        MensajesDialogAlert.mensajeError(getContext(),"ERROR", "NO PRESENTA REBOTES");

                         articuloReboteAdapter = new ArticuloReboteAdapter(listaAdapter, false, false, true, true);
                        reciclador.setLayoutManager(lManager);
                        reciclador.setAdapter(articuloReboteAdapter);

                    }else {

                        listaAdapter.add(listaSeccionBonif);
                        listaAdapter.add(listaSeccionNormal);
                        listaAdapter.add(listaSeccionRef);

                        articuloReboteAdapter = new ArticuloReboteAdapter(listaAdapter, false, false, true, true);
                        reciclador.setLayoutManager(new StickyHeaderLayoutManager());
                        reciclador.setAdapter(articuloReboteAdapter);
                    }
                    progressDialog.dismiss();
//
                }
            }

            @Override
            public void onFailure(Call<JsonRespuesta<ArticuloRebote>> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(getView(), "Problemas de conexión  " , Snackbar.LENGTH_INDEFINITE)
                        .setAction("Reintentar", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // cargarPlanillaCobranza();

                            }
                        }).show();
                //
            }
        });
    }


    public interface InterfaceComunicacion{

        public void reloadInv();
    }

}
