package com.legado.distribuciongps.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.activities.PedidoActivity;
import com.legado.distribuciongps.util.Metodos;

public class MyDialogEdit extends DialogFragment {
    private static final String TAG = "MyDialogEdit";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    public static MyDialogEdit newInstance(String cantidad,String cantidadBase, String codigoItem,int position ) {//,CarritoFragment carritoFragment
        MyDialogEdit fragment = new MyDialogEdit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, cantidad);
        args.putString(ARG_PARAM2, codigoItem);
        args.putInt(ARG_PARAM3, position);
        args.putString(ARG_PARAM4,cantidadBase);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_dialog_edit, container, false);
        getDialog().setTitle("Edit Dialog");

        final EditText txtCantidad = (EditText) rootView.findViewById(R.id.txtCantidadEdit);

        txtCantidad.setText(getArguments().getString(ARG_PARAM1));
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        txtCantidad.requestFocus();

        Button dismiss = (Button) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Button btnActualizar = (Button) rootView.findViewById(R.id.btnActualizar);
        btnActualizar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Integer base = Integer.parseInt(getArguments().getString(ARG_PARAM4));//siempre es numero.
                if(Metodos.verificarNumero(txtCantidad.getText().toString())){
                    Integer nueva = Integer.parseInt(txtCantidad.getText().toString());

                    if(nueva > 0){
                        if(nueva <= base){
                            PedidoActivity.detalles.get(getArguments().getInt(ARG_PARAM3)).setCantidadNueva(nueva);
                            PedidoActivity.Actualizar(getArguments().getInt(ARG_PARAM3));
                            dismiss();
                        } else {
                            Toast.makeText(getContext(),"Cantidad no debe de ser mayor a "+base+".",Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(),"Cantidad debe de ser mayor a cero.",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getContext(),"Cantidad Incorrecta",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rootView;
    }

}
