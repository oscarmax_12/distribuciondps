package com.legado.distribuciongps.dialogs;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import com.airbnb.lottie.LottieAnimationView;
import com.legado.distribuciongps.R;


/**
 * Created by __Adrian__ on 20/6/2017.
 */

public class DialogProgress extends DialogFragment {

         public static DialogProgress newInstance() {
             DialogProgress fragment= new DialogProgress();
             return  fragment;
        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_dialog_loading, container, false);
        final LottieAnimationView animationView= (LottieAnimationView) rootView.findViewById(R.id.loadingData);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        animationView.loop(true);
        animationView.playAnimation();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener()
        {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, android.view.KeyEvent event) {
                if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
                {


                    return true; // pretend we've processed it
                }
                else
                    return false; // pass on to be processed as normal
            }
        });

        return rootView;
    }



}
