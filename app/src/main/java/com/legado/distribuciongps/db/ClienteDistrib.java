package com.legado.distribuciongps.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "CLIENTE_DISTRIB".
 */
@Entity
public class ClienteDistrib {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String cod_cliente;
    private String desc_cliente;
    private String coordenadas;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public ClienteDistrib() {
    }

    public ClienteDistrib(Long id) {
        this.id = id;
    }

    @Generated
    public ClienteDistrib(Long id, String cod_cliente, String desc_cliente, String coordenadas) {
        this.id = id;
        this.cod_cliente = cod_cliente;
        this.desc_cliente = desc_cliente;
        this.coordenadas = coordenadas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getCod_cliente() {
        return cod_cliente;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCod_cliente(@NotNull String cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public String getDesc_cliente() {
        return desc_cliente;
    }

    public void setDesc_cliente(String desc_cliente) {
        this.desc_cliente = desc_cliente;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
