package com.legado.distribuciongps.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.AppLaunchChecker;
import androidx.core.content.ContextCompat;

import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.ApiError;
import com.legado.distribuciongps.modelo.Asignacion;
import com.legado.distribuciongps.modelo.Usuario;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SedesAdapter;
import com.legado.distribuciongps.util.SessionUsuario;

import java.io.IOException;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";

    String imei;

    public static ArrayList<String> numbers;

    static final Integer PHONESTATS = 0x1;


    @BindView(R.id.etUsuario)
    EditText etUsuario;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.float_label_username)
    TextInputLayout float_label_username;
    @BindView(R.id.float_label_password)
    TextInputLayout float_label_password;
    @BindView(R.id.login_progress)
    View login_progress;

    @BindView(R.id.logo_gps)
    ImageView logo_gps;
    @BindView(R.id.login_form)
    View login_form;
    @BindView(R.id.txtMac)
    TextView txtMac;
    SessionUsuario sessionUsuario;
    @BindView(R.id.spinnerSede)
    Spinner spinnerSede;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        try {
            sessionUsuario = new SessionUsuario(this);
            progressDialog = new ProgressDialog(this, R.style.AppTheme_MyDialog);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);

//            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            String[] perms = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
            consultarPermiso2("asd",PHONESTATS);
//            consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
//            consultarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, PHONESTATS);

            // String mac = Metodos.parsingEmptyUppercase(getMacAddress());
            //txtMac.setText(mac);
            //sessionUsuario.CargarCodigo(mac);
            etUsuario.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        attemptLogin();
                        handled = true;
                    }
                    return handled;
                }
            });
            etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
                        if (!isOnline()) {
                            showLoginError(getString(R.string.error_network));
                            return false;
                        }
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });
            showProgress(true);
            //progressDialog.show();
            Call<JsonRespuesta<Asignacion>> call = ApiRetrofit.getInstance().getLoginService().CargarSedes();
            call.enqueue(new Callback<JsonRespuesta<Asignacion>>() {
                @Override
                public void onResponse(Call<JsonRespuesta<Asignacion>> call, retrofit2.Response<JsonRespuesta<Asignacion>> response) {
                    JsonRespuesta<Asignacion> respuesta = response.body();

                    Integer estado = respuesta.getEstado();

                    if (estado == 1) {
                        List<Asignacion> sedes = respuesta.getData();
                        Log.e("sedesTag",sedes.toString());
                        loadSpinnerSedes(sedes);
                        //progressDialog.dismiss();
                        showProgress(false);
                    } else {
                        Toast.makeText(getApplicationContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
                        // progressDialog.dismiss();
                        showProgress(false);
                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta<Asignacion>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
                    //progressDialog.dismiss();
                    showProgress(false);
                }
            });

            pedirPermisoUbicacion();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOnline()) {
                    showLoginError(getString(R.string.error_network));
                    return;
                }
                attemptLogin();
            }
        });
        etUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    // Toast.makeText(getApplicationContext(), "Got the focus", Toast.LENGTH_LONG).show();
                } else {
                    verificarFocus();
                }
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    // Toast.makeText(getApplicationContext(), "Got the focus", Toast.LENGTH_LONG).show();
                } else {
                    verificarFocus();
                }
            }
        });
        etUsuario.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                verificarFocus();
                return false;
            }
        });
        etUsuario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                verificarFocus();

                Log.e("char login", charSequence.toString().toUpperCase());
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    etUsuario.setText(s);
                }
                etUsuario.setSelection(etUsuario.getText().length());
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                verificarFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void pedirPermisoUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            MensajesDialogAlert.mensajeError(getBaseContext(), "ERROR", "DEBE PERMITIR EL ACCESO A LA UBICACION");

            return;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // Validamos si el usuario acepta el permiso para que la aplicación acceda a los datos internos del equipo, si no denegamos el acceso
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    imei = obtenerIMEI();
                } else {

                    Toast.makeText(this, "Has negado el permiso a la aplicación", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void consultarPermiso(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        } else {
            imei = obtenerIMEI();
            Toast.makeText(this, permission + " El permiso a la aplicación esta concedido.", Toast.LENGTH_SHORT).show();
        }
    }
    private void consultarPermiso2(String permission, Integer requestCode) {
        String[] perms = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,  Manifest.permission.ACCESS_COARSE_LOCATION)||
                        ActivityCompat.shouldShowRequestPermissionRationale(this,  Manifest.permission.READ_PHONE_STATE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this,  Manifest.permission.ACCESS_FINE_LOCATION) ){

                    ActivityCompat.requestPermissions(this, perms, requestCode);

                } else {

                    ActivityCompat.requestPermissions(this, perms, requestCode);
                }
            } else {
                imei = obtenerIMEI();
                Toast.makeText(this, permission + " El permiso a la aplicación esta concedido.", Toast.LENGTH_SHORT).show();
            }


    }

    private String obtenerNumCel() {
        TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber=null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (checkSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);
                }
            }
            mPhoneNumber= tMgr.getLine1Number();

        }
        return mPhoneNumber;
 }
    private String obtenerIMEI() {
        final TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0x1);
                }
            }
            return telephonyManager.getImei();
        } else {
            return telephonyManager.getDeviceId();
        }

    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String par = Integer.toHexString(b & 0xFF) + ":";
                    if (par.length() < 3) {
                        par = "0" + par; // pad with leading zero if needed
                    }
                    res1.append(par);
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.e("Error", ex.getMessage());
        }
        return "";
    }

    public void verificarFocus() {
        float_label_username.setError(null);
        float_label_password.setError(null);
        final String username = etUsuario.getText().toString();
        String password = etPassword.getText().toString();
        //if (TextUtils.isEmpty(password)) {
        // float_label_password.setError(getString(R.string.error_field_required));
        //} //else if (!isPasswordValid(password)) {
        // float_label_password.setError(getString(R.string.error_invalid_password));
        //}
        if (TextUtils.isEmpty(username)) {
            float_label_username.setError(getString(R.string.error_field_required));
        } else if (!isUserXrayValid(username)) {
            float_label_username.setError(getString(R.string.error_invalid_user_id));
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginFailed() {
        if (etUsuario.getText().toString().isEmpty()) {
            etUsuario.requestFocus();
        } else if (etPassword.getText().toString().isEmpty()) {
            etPassword.requestFocus();
        } else {
            Toast.makeText(getBaseContext(), "DATOS INCORRECTOS", Toast.LENGTH_LONG).show();
            etUsuario.requestFocus();
        }
        btnLogin.setEnabled(true);
    }

    private void attemptLogin() {
        float_label_username.setError(null);
        float_label_password.setError(null);

        final String username = etUsuario.getText().toString();
        // String password = etPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;
       /* // Check for a valid password, if the user entered one.
       // if (TextUtils.isEmpty(password)) {
          //  float_label_password.setError(getString(R.string.error_field_required));
            //focusView = float_label_password;
          //  cancel = true;
      //  } else if (!isPasswordValid(password)) {
            float_label_password.setError(getString(R.string.error_invalid_password));
            focusView = float_label_password;
            cancel = true;
        }*/
        // Verificar si el ID tiene contenido.
        if (TextUtils.isEmpty(username)) {
            float_label_username.setError(getString(R.string.error_field_required));
            focusView = float_label_username;
            cancel = true;
        } else if (!isUserXrayValid(username)) {
            float_label_username.setError(getString(R.string.error_invalid_user_id));
            focusView = float_label_username;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            // Mostrar el indicador de carga y luego iniciar la petición asíncrona.
            showProgress(true);

            Map<String, String> dataConsulta = new HashMap<>();
            Asignacion asignacion = (Asignacion) spinnerSede.getSelectedItem();
            dataConsulta.put("codemp", asignacion.getEmpresa().getCodEmpresa());
            dataConsulta.put("codsede", asignacion.getSede().getCodSede());
            dataConsulta.put("numplaca", username);
            Log.e("Error", asignacion.getEmpresa().getCodEmpresa());
            Log.e("Error", asignacion.getSede().getCodSede());
            Log.e("Error", asignacion.getSede().getCodDepartamento());
            Log.e("Error", username);
            Call<JsonRespuesta<Usuario>> loginCall = ApiRetrofit.getInstance().getLoginService().loginxPlaca(dataConsulta);
            loginCall.enqueue(new Callback<JsonRespuesta<Usuario>>() {
                @Override
                public void onResponse(Call<JsonRespuesta<Usuario>> call, Response<JsonRespuesta<Usuario>> response) {
                    //Procesar errores
                    if (!response.isSuccessful()) {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        if (response.errorBody().contentType().subtype().equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());
                            error = apiError.getMessage();
                            Log.d("LoginActivity", apiError.getDeveloperMessage());
                        } else {
                            try {
                                // Reportar causas de error no relacionado con la API
                                Log.d("LoginActivity", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        showLoginError(error);
                        return;
                    }
                    if (response.body().getEstado() == 1) {
                        Usuario usuario = response.body().getItem();
                        List<Asignacion> listas = usuario.getAsignaciones();
                        System.out.println("OLA");
                        System.out.println(usuario.getNick());
                        System.out.println(usuario.getNombre());
                        sessionUsuario.IniciarSession(true, usuario, username, asignacion.getSede().getCodSede(),asignacion.getSede().getCodDepartamento());
                        showAppointmentsScreen();
                    } else {
                        showLoginError(response.body().getMensaje());
                        showProgress(false);
                    }
                }

                @Override
                public void onFailure(Call<JsonRespuesta<Usuario>> call, Throwable t) {
                    showProgress(false);
                    showLoginError(t.getMessage());
                }
            });

        }
    }

    private boolean isUserXrayValid(String username) {
        return username.length() == 7;
    }

   /* private boolean isPasswordValid(String password) {
        return password.length() > 0;
    }*/

    private void showLoginError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private void showProgress(boolean show) {
        login_progress.setVisibility(show ? View.VISIBLE : View.GONE);
        int visibility = show ? View.GONE : View.VISIBLE;
        logo_gps.setVisibility(visibility);
        login_form.setVisibility(visibility);
    }

    private void showAppointmentsScreen() {
        startActivity(new Intent(this, InicioActivity.class));
        Metodos.validarSesion(this);
        finish();
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void loadSpinnerSedes(List<Asignacion> asignaciones) {
        SedesAdapter vendedorSpinnerAdapter = new SedesAdapter(this, R.id.descEmpresa, asignaciones);
        vendedorSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSede.setAdapter(vendedorSpinnerAdapter);
    }

}