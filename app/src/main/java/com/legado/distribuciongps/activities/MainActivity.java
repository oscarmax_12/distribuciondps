package com.legado.distribuciongps.activities;

import android.app.DatePickerDialog;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.res.Resources;

import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBar;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.GenerarPedidoFragment;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;
import com.legado.distribuciongps.util.MensajesDialogAlert;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Locale;

import butterknife.ButterKnife;

import static com.legado.distribuciongps.R.id.menu3;

public class MainActivity extends BaseActivity {

    private static BluetoothSocket Abtsocket;
    private static OutputStream AoutputStream;
    private GenerarPedidoFragment fragment = null;
    private int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbar();
        fragment = new GenerarPedidoFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.content_main, fragment);
        ft.commit();
    }

    @Override
    public boolean providesActivityToolbar() {
        return true;
    }

    private void setupToolbar() {
        final ActionBar ab = getActionBarToolbar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawer();
                return true;
            case R.id.verPlacas:
//                VerPlacas();
                return true;
            case R.id.verUrl:
//                VerUrls();
                return true;
            case R.id.logout:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    private void logout() {
        sessionUsuario.IniciarSession(false, null, "", "","");//desconectarme
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

//    private void VerPlacas(){
//        final Asignacion asignacion = (Asignacion) fragment.getSpinnerEmpresas().getSelectedItem();
//        fragment.getProgressDialog().show();
//        Map<String, String> dataClientes = new HashMap<>();
//        dataClientes.put("codemp", asignacion.getEmpresa() != null ? asignacion.getEmpresa().getCodEmpresa().toUpperCase() : "00");
//        dataClientes.put("codsede", asignacion.getSede() != null ? asignacion.getSede().getCodSede().toUpperCase() : "00");
//        dataClientes.put("fecha","");
//
//        Call<JsonRespuesta<Placa>> call = ApiRetrofit.getInstance().getPlacaService().listarPlacasxSede(dataClientes);
//        call.enqueue(new Callback<JsonRespuesta<Placa>>() {
//            @Override
//            public void onResponse(Call<JsonRespuesta<Placa>> call, retrofit2.Response<JsonRespuesta<Placa>> response) {
//                JsonRespuesta<Placa> respuesta = response.body();
//                List<Placa> placas = respuesta.getData();
//                Integer estado = respuesta.getEstado();
//
//                if (estado == 1) {
//                    Bundle bundle = new Bundle();
//                    PlacasFragment p = PlacasFragment.newInstance(bundle,placas,fragment);
//                    FragmentManager fragmentManager = getSupportFragmentManager();
//                    //p.getDialog().setTitle("LISTA DE PLACAS");
//                    p.show(fragmentManager,"Lista de Url");
//                    fragment.getProgressDialog().dismiss();
//                } else {
//                    Toast.makeText(getApplicationContext(), respuesta.getMensaje(), Toast.LENGTH_LONG).show();
//                    fragment.getProgressDialog().dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonRespuesta<Placa>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "NO HAY CONEXION CON EL SERVIDOR", Toast.LENGTH_LONG).show();
//                fragment.getProgressDialog().dismiss();
//            }
//        });
//    }

//    private void VerUrls(){
//        Bundle bundle = new Bundle();
//        //Placa placa1 = new Placa("http://190.223.55.172:8080/serviciodistribucionv3/distribucion/");
//        Placa placa1 = new Placa("http://172.17.55.21:8080/distribucion/");
//        Placa placa2 = new Placa("http://172.17.55.21:8080/distribucion/");
//        List<Placa> placas = new ArrayList<Placa>();
//        placas.add(placa1);
//        placas.add(placa2);
//        UrlFragment p = UrlFragment.newInstance(bundle,placas,fragment) ;
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        p.show(fragmentManager,"Lista de Urls");
//        fragment.getProgressDialog().dismiss();
//    }

    @Override
    protected void goToNavDrawerItem(int item) {
        switch (item) {
            case R.id.menu0:
                Intent intent0 = new Intent(this, InicioActivity.class);
                startActivity(intent0);
                finish();
                break;
            case R.id.menu1:
                Intent pantalla = new Intent(this, toFrameMapa.class);
                Bundle b = new Bundle();
                b.putString("placa", sessionUsuario.getCodigoAplicacion());
                b.putString("op", "lista");
                pantalla.putExtras(b);
                startActivity(pantalla);

                break;
            case R.id.menu2:
                Intent intent2 = new Intent(this, MisEntregasActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.menu3:
                Intent intent3 = new Intent(this, TabsReboteActivity.class);
                startActivity(intent3);
                finish();
                break;
//            case R.id.menu4:
//                Intent intent4 = new Intent(this, ReboteConfirmacionActivity.class);
//                startActivity(intent4);
//                finish();
//                break;
            case R.id.menu5:
                Intent intent5 = new Intent(this, toFrameMapa.class);
                Bundle b1 = new Bundle();
                b1.putString("op", "facturar");
                b1.putString("placa", "nada");
                intent5.putExtras(b1);
                startActivity(intent5);

                break;
            case R.id.menu7:
                Intent intent7 = new Intent(this, PorLiquidarActivity.class);
                Bundle b2 = new Bundle();
                b2.putString("placa", sessionUsuario.getCodigoAplicacion());
                intent7.putExtras(b2);
                startActivity(intent7);

                break;
            case R.id.btnCerrarSesion:
                logout();
                stopService();
                break;

        }
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, VerificacionMontoService.class);
        serviceIntent.setAction("stop");
        stopService(serviceIntent);
    }

    public void FechaDialog() {
        /*PARA EDITAR EL LENGUAGE DEL CALENDARIO*/
        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);


        Resources res = this.getResources();
// Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale("es");//CAMBIAR EL LO RESOURCES SI SE QUIERE TENER EN VARIOS IDIOMAS
        res.updateConfiguration(conf, dm);



        /*PARA EDITAR EL LENGUAGE DEL CALENDARIO*/
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.AppTheme_MyDialog,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        Intent pantalla = new Intent(MainActivity.this, toFrameMapa.class);
                        Bundle b = new Bundle();
                        b.putString("fecha", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        b.putString("placa", sessionUsuario.getCodigoAplicacion());
                        pantalla.putExtras(b);
                        MensajesDialogAlert.mensajeInformacion(MainActivity.this, "Fecha selecionada", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year + " Con Placa: " + sessionUsuario.getCodigoAplicacion());
                        startActivity(pantalla);
//                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (Abtsocket != null) {
                AoutputStream.close();
                Abtsocket.close();
                Abtsocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}