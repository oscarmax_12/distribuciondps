package com.legado.distribuciongps.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.legado.distribuciongps.BuildConfig;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.api.ApiRetrofit;
import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.mapservice.VerificacionMontoService;
import com.legado.distribuciongps.modelo.Version;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;
import com.legado.distribuciongps.util.SessionUsuario;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by _ADRIAN_ on 28/02/2017.
 */

public class SplashActivity extends Activity {
    private static int SPLASH_TIME_OUT = 5000;
    public static final String SPLASH_SCREEN_OPTION_1 = "Option 1";
    public static final String SPLASH_SCREEN_OPTION_2 = "Option 2";
    public static final String SPLASH_SCREEN_OPTION_3 = "Option 3";
    int versionCode = BuildConfig.VERSION_CODE;
    String versionName = BuildConfig.VERSION_NAME;
    SessionUsuario sessionUsuario;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.etWelcome)
    TextView etWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sessionUsuario = new SessionUsuario(this);

        setAnimation(SPLASH_SCREEN_OPTION_3);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                validarSesion();

            }
        }, SPLASH_TIME_OUT);

    }

    public void validarSesion() {

        Version objVersion = new Version();
        objVersion.setDescripcion("DISTRIBUCION");
        objVersion.setVersion_app(versionName);
        objVersion.setCodSede(null);
        objVersion.setImei(null);
        objVersion.setPlaca(null);
        Gson gson = new Gson();

        String logRepartoJson = gson.toJson(objVersion);
        Log.e("validarVersion", logRepartoJson);
        Call<JsonRespuesta> call = ApiRetrofit.getInstance().getVersionService().ValidarVersion(objVersion);
        call.enqueue(new Callback<JsonRespuesta>() {
            @Override
            public void onResponse(Call<JsonRespuesta> call, Response<JsonRespuesta> response) {
                if (response.body().getEstado() >= 1) {
                    Metodos.IS_VALIDATE_VERSION="true";
                    if (sessionUsuario.getSession()) {
                        Intent i = new Intent(SplashActivity.this, InicioActivity.class);
                        startActivity(i);
                        startService();
                        finish();
                    } else {
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    MensajesDialogAlert.mensajeError(SplashActivity.this, "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
                    SplashActivity.this.finish();
                    Metodos.IS_VALIDATE_VERSION="false";
                    //   progressDialog.dismiss();

                }
            }
            @Override
            public void onFailure(Call<JsonRespuesta> call, Throwable t) {

                Toast.makeText(SplashActivity.this, "PROBLEMAS DE CONEXION", Toast.LENGTH_LONG).show();
                //
            }
        });
    }

    //    @Override
//    protected void onPause() {
//        super.onPause();
//        // hidePDialog();
//    }
    public void startService() {
        Intent serviceIntent = new Intent(this, VerificacionMontoService.class);
//    serviceIntent.putExtra("inputExtra", "Servicio se inicio");
        serviceIntent.setAction("start");
        Log.e("servicio", "comenzo el service");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            SplashActivity.this.startForegroundService(serviceIntent);
        } else {
            startService(serviceIntent);
        }

    }

    private void setAnimation(String category) {
        if (category.equals(SPLASH_SCREEN_OPTION_1)) {
            animation1();
        } else if (category.equals(SPLASH_SCREEN_OPTION_2)) {
            animation2();
        } else if (category.equals(SPLASH_SCREEN_OPTION_3)) {
            animation2();
            animation3();
        }
    }


    private void animation1() {
        ObjectAnimator scaleXAnimation = ObjectAnimator.ofFloat(logo, "scaleX", 5.0F, 1.0F);
        scaleXAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleXAnimation.setDuration(1200);
        ObjectAnimator scaleYAnimation = ObjectAnimator.ofFloat(logo, "scaleY", 5.0F, 1.0F);
        scaleYAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleYAnimation.setDuration(1200);
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(logo, "alpha", 0.0F, 1.0F);
        alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimation.setDuration(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
        animatorSet.setStartDelay(500);
        animatorSet.start();
    }

    private void animation2() {
        logo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center);
        logo.startAnimation(anim);
    }

    private void animation3() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(etWelcome, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1700);
        alphaAnimation.setDuration(500);
        alphaAnimation.start();
    }

}
