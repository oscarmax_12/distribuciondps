package com.legado.distribuciongps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.Toast;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.fragments.listado_productos;
import com.legado.distribuciongps.util.MensajesDialogAlert;
import com.legado.distribuciongps.util.Metodos;

public class act_productos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_porductos);



Metodos.validarSesion(this);
        if (Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("false")){
            MensajesDialogAlert.mensajeError(this, "ACTUALIZAR!", "Hay una actualización, comunicarse con soporte");
            this.finish();
        }else if(Metodos.IS_VALIDATE_VERSION.equalsIgnoreCase("ERROR")){
            MensajesDialogAlert.mensajeError(this, "ERROR!", "Error");
            this.finish();
        }
        Bundle b = getIntent().getExtras();
        String nroPedido = "",montoTotal,flag10="",codEmpresaTemp="",tipoDoc="",seriePre="",codLoc=""; // or other values
        if(b != null){
            nroPedido = b.getString("nroPedido");
            montoTotal = b.getString("montoTotal");
            flag10 = b.getString("flag10");
            codEmpresaTemp = b.getString("codEmpresaTemp");
            tipoDoc = b.getString("tipoDoc");
            seriePre = b.getString("seriePre");
            codLoc = b.getString("codLoc");


            //PASO DE PARAMETROS AL FRAGMENT
            listado_productos inicioFragment = new listado_productos();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle data = new Bundle();
            data.putString("nroPedido", nroPedido);
            data.putString("montoTotal", montoTotal);
            data.putString("flag10",flag10);
            data.putString("codEmpresaTemp",codEmpresaTemp);
            data.putString("tipoDoc", tipoDoc);
            data.putString("seriePre", seriePre);
            data.putString("codLoc", codLoc);
            data.putString("codCliente",b.getString("codCliente"));
//            inicioFragment.setInitialSavedState(appState.getMyFragmentState());
//            getSupportFragmentManager().saveFragmentInstanceState(inicioFragment);
            inicioFragment.setArguments(data);
            fragmentTransaction.replace(R.id.contenedorProductos, inicioFragment);
            fragmentTransaction.commit();

        }else{
            Toast.makeText(this,"Error bundle",Toast.LENGTH_LONG).show();
        }
    }


}
