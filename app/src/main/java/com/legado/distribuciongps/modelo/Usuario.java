package com.legado.distribuciongps.modelo;

import java.util.List;

/**
 * Created by ERICK on 05/06/2018.
 */

public class Usuario {

    private String nick;
    private String nombre;
    private List<Asignacion> asignaciones;

    public Usuario() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Asignacion> getAsignaciones() {
        return asignaciones;
    }

    public void setAsignaciones(List<Asignacion> asignaciones) {
        this.asignaciones = asignaciones;
    }
}
