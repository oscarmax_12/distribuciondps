package com.legado.distribuciongps.modelo;

public class Coordenadas {
    private String Latitud;
    private String Longitud;

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }
}
