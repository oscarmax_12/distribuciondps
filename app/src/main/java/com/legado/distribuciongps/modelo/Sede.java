package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 06/06/2018.
 */

public class Sede {

    private String codSede;
    private String descSede;
    private String codDepartamento;

    public Sede() {

    }

    public Sede(String codSede, String descSede) {
        this.codSede = codSede;
        this.descSede = descSede;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getCodSede() {
        return codSede;
    }

    public void setCodSede(String codSede) {
        this.codSede = codSede;
    }

    public String getDescSede() {
        return descSede;
    }

    public void setDescSede(String descSede) {
        this.descSede = descSede;
    }
}