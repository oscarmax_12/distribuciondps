package com.legado.distribuciongps.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**Created by ERICK on 07/06/2018.
 * and edited by OSCAR on 19/09/2019*/
public class Pedido {

    private Empresa empresa;
    private Sede sede;
    private Mesa mesa;
    private Canal canal;
    private String numPedido;
    private String codCondicion;
    private String codLocalidad;
    private Cliente cliente;
    private String codLista;
    private BigDecimal montoTotal;
    private Vendedor vendedor;
    private Almacen almacen;
    private String numPedidoRef;
    private List<Articulo> detalles;
    private String estado;
    private Double porcDesc;
    private String tipDoc;
    private String seriePre;
    private String placa;
    private String ruta;
    private String fecha;
    private String anulados;
    private String codRuta;
    private String tipoPago;//CONTADO CREDITO PERO SOLO VISTA
    private String flag10;
    private String motivo;
    private String codEmpresaTemp;
    private String  nroPacking;

    private String coordX;
    private String coordY;

    public String getCoordX() {
        return coordX;
    }

    public void setCoordX(String coordX) {
        this.coordX = coordX;
    }

    public String getCoordY() {
        return coordY;
    }

    public void setCoordY(String coordY) {
        this.coordY = coordY;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getCodEmpresaTemp() {
        return codEmpresaTemp;
    }

    public void setCodEmpresaTemp(String codEmpresaTemp) {
        this.codEmpresaTemp = codEmpresaTemp;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getFlag10() {
        return flag10;
    }

    public void setFlag10(String flag10) {
        this.flag10 = flag10;
    }

    public static ArrayList<Pedido> listaPedido= new ArrayList<Pedido>();

    public String getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(String numPedido) {
        this.numPedido = numPedido;
    }

    public String getCodCondicion() {
        return codCondicion;
    }

    public void setCodCondicion(String codCondicion) {
        this.codCondicion = codCondicion;
    }

    public String getCodLocalidad() {
        return codLocalidad;
    }

    public void setCodLocalidad(String codLocalidad) {
        this.codLocalidad = codLocalidad;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getCodLista() {
        return codLista;
    }

    public void setCodLista(String codLista) {
        this.codLista = codLista;
    }

    public List<Articulo> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Articulo> detalles) {
        this.detalles = detalles;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getNumPedidoRef() {
        return numPedidoRef;
    }

    public void setNumPedidoRef(String numPedidoRef) {
        this.numPedidoRef = numPedidoRef;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Canal getCanal() {
        return canal;
    }

    public void setCanal(Canal canal) {
        this.canal = canal;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(Double porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getSeriePre() {
        return seriePre;
    }

    public void setSeriePre(String seriePre) {
        this.seriePre = seriePre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getAnulados() {
        return anulados;
    }

    public void setAnulados(String anulados) {
        this.anulados = anulados;
    }

    public String getCodRuta() {
        return codRuta;
    }

    public void setCodRuta(String codRuta) {
        this.codRuta = codRuta;
    }
}
