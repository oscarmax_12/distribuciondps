package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 06/06/2018.
 */

public class Empresa {

    private String codEmpresa;
    private String descEmpresa;
    private String ruc;
    private String direccion;

    public Empresa() {
    }

    public Empresa(String codEmpresa, String descEmpresa) {
        this.codEmpresa = codEmpresa;
        this.descEmpresa = descEmpresa;
    }

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getDescEmpresa() {
        return descEmpresa;
    }

    public void setDescEmpresa(String descEmpresa) {
        this.descEmpresa = descEmpresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
