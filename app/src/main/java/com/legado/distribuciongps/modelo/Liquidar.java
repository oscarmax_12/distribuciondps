package com.legado.distribuciongps.modelo;

public class Liquidar {
    private String  docsAsignadosCantLiquidar;
    private String  rbtcantLiquidar;
    private String  rpcantLiquidar;
    private String  docsentregadosCantLiquidar;
    private String  MontoSolesLiquidar;
    private String  packingsLiquidar;
    private String  codSedeLiquidar;
    private String  codEmpresaLiquidar;
    private int flag_liquidado;

    public String getDocsAsignadosCantLiquidar() {
        return docsAsignadosCantLiquidar;
    }

    public void setDocsAsignadosCantLiquidar(String docsAsignadosCantLiquidar) {
        this.docsAsignadosCantLiquidar = docsAsignadosCantLiquidar;
    }

    public String getRbtcantLiquidar() {
        return rbtcantLiquidar;
    }

    public void setRbtcantLiquidar(String rbtcantLiquidar) {
        this.rbtcantLiquidar = rbtcantLiquidar;
    }

    public String getRpcantLiquidar() {
        return rpcantLiquidar;
    }

    public void setRpcantLiquidar(String rpcantLiquidar) {
        this.rpcantLiquidar = rpcantLiquidar;
    }

    public String getDocsentregadosCantLiquidar() {
        return docsentregadosCantLiquidar;
    }

    public void setDocsentregadosCantLiquidar(String docsentregadosCantLiquidar) {
        this.docsentregadosCantLiquidar = docsentregadosCantLiquidar;
    }

    public String getMontoSolesLiquidar() {
        return MontoSolesLiquidar;
    }

    public void setMontoSolesLiquidar(String montoSolesLiquidar) {
        MontoSolesLiquidar = montoSolesLiquidar;
    }

    public String getPackingsLiquidar() {
        return packingsLiquidar;
    }

    public void setPackingsLiquidar(String packingsLiquidar) {
        this.packingsLiquidar = packingsLiquidar;
    }

    public String getCodSedeLiquidar() {
        return codSedeLiquidar;
    }

    public void setCodSedeLiquidar(String codSedeLiquidar) {
        this.codSedeLiquidar = codSedeLiquidar;
    }

    public String getCodEmpresaLiquidar() {
        return codEmpresaLiquidar;
    }

    public void setCodEmpresaLiquidar(String codEmpresaLiquidar) {
        this.codEmpresaLiquidar = codEmpresaLiquidar;
    }

    public int getFlag_liquidado() {
        return flag_liquidado;
    }

    public void setFlag_liquidado(int flag_liquidado) {
        this.flag_liquidado = flag_liquidado;
    }
}
