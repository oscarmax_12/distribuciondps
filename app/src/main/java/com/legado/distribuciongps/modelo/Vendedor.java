/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.legado.distribuciongps.modelo;


import com.google.gson.annotations.SerializedName;

/**
 *
 * @author CIXTIC03
 */

public class Vendedor {
    @SerializedName("cod")
  private String codVendedor;
    @SerializedName("desc")
  private String descVendedor;
    @SerializedName("user")
    private String usuario;
    @SerializedName("pass")
  private String passVendedor;


    public Vendedor() {
    }

    public Vendedor(String codVendedor, String descVendedor, String usuario , String passVendedor) {
        this.codVendedor = codVendedor;
        this.descVendedor = descVendedor;
        this.usuario=usuario;
        this.passVendedor = passVendedor;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public String getDescVendedor() {
        return descVendedor;
    }

    public void setDescVendedor(String descVendedor) {
        this.descVendedor = descVendedor;
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassVendedor() {
        return passVendedor;
    }

    public void setPassVendedor(String passVendedor) {
        this.passVendedor = passVendedor;
    }
}
