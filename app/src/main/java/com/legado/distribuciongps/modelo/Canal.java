package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 08/06/2018.
 */

public class Canal {

    private String codCanal;
    private String descCanal;

    public String getCodCanal() {
        return codCanal;
    }

    public void setCodCanal(String codCanal) {
        this.codCanal = codCanal;
    }

    public String getDescCanal() {
        return descCanal;
    }

    public void setDescCanal(String descCanal) {
        this.descCanal = descCanal;
    }
}
