package com.legado.distribuciongps.modelo;

import java.util.ArrayList;

/**
 * Created by ERICK on 07/06/2018.
 */

public class Cliente {

    private String codCliente;
    private String nomCliente;
    private String rucCliente;
    private String dirDespacho;
    private String Packings;
    private String cantPedidos;
    private String tienerebotes;
    private String flagEstadoEntregadoRebotes;
    public static ArrayList<Cliente> listaCliente = new ArrayList<>();

    public String getFlagEstadoEntregadoRebotes() {
        return flagEstadoEntregadoRebotes;
    }

    public void setFlagEstadoEntregadoRebotes(String flagEstadoEntregadoRebotes) {
        this.flagEstadoEntregadoRebotes = flagEstadoEntregadoRebotes;
    }

    public String getTienerebotes() {
        return tienerebotes;
    }

    public void setTienerebotes(String tienerebotes) {
        this.tienerebotes = tienerebotes;
    }

    public String getCantPedidos() {
        return cantPedidos;
    }

    public void setCantPedidos(String cantPedidos) {
        this.cantPedidos = cantPedidos;
    }

    public String getPackings() {
        return Packings;
    }

    public void setPackings(String packings) {
        Packings = packings;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getRucCliente() {
        return rucCliente;
    }

    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    public String getDirDespacho() {
        return dirDespacho;
    }

    public void setDirDespacho(String dirDespacho) {
        this.dirDespacho = dirDespacho;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }
}
