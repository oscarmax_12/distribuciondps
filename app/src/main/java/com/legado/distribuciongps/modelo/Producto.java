package com.legado.distribuciongps.modelo;

import java.util.ArrayList;

public class Producto {
    private String nombreProducto;
    private String codProd;
    private String montoProd;
    private String cantProd;
    private boolean llevar;
    private int cantProdEditar;
    private String uniMedida;
    //PARA SACAR EL MONTO
    private String nroPedido;

    public String getNroPedido() {
        return nroPedido;
    }

    public void setNroPedido(String nroPedido) {
        this.nroPedido = nroPedido;
    }

    public static ArrayList<Producto> listaProductos= new ArrayList<Producto>();

    public String getUniMedida() {
        return uniMedida;
    }

    public void setUniMedida(String uniMedida) {
        this.uniMedida = uniMedida;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getCodProd() {
        return codProd;
    }

    public void setCodProd(String codProd) {
        this.codProd = codProd;
    }

    public String getMontoProd() {
        return montoProd;
    }

    public void setMontoProd(String montoProd) {
        this.montoProd = montoProd;
    }

    public String getCantProd() {
        return cantProd;
    }

    public void setCantProd(String cantProd) {
        this.cantProd = cantProd;
    }

    public boolean isLlevar() {
        return llevar;
    }

    public void setLlevar(boolean llevar) {
        this.llevar = llevar;
    }

    public int getCantProdEditar() {
        return cantProdEditar;
    }

    public void setCantProdEditar(int cantProdEditar) {
        this.cantProdEditar = cantProdEditar;
    }
}
