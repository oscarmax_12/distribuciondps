package com.legado.distribuciongps.modelo;

public class Entrega {
    private String  codEmpresa;
    private String  tipoDoc;
    private String  numSerie;
    private String  numPreImpreso;
    private String  codUsuario;
    private String  coordX;
    private String  coordY;
    private int flag_cobro;

    public int getFlag_cobro() {
        return flag_cobro;
    }

    public void setFlag_cobro(int flag_cobro) {
        this.flag_cobro = flag_cobro;
    }

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public String getNumPreImpreso() {
        return numPreImpreso;
    }

    public void setNumPreImpreso(String numPreImpreso) {
        this.numPreImpreso = numPreImpreso;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getCoordX() {
        return coordX;
    }

    public void setCoordX(String coordX) {
        this.coordX = coordX;
    }

    public String getCoordY() {
        return coordY;
    }

    public void setCoordY(String coordY) {
        this.coordY = coordY;
    }
}
