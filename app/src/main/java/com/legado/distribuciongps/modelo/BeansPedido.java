package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 02/07/2018.
 */

public class BeansPedido {


    private String nro_pedido;
    private String nro_back_order;
    private String cod_item;
    private String proveedor ;
    private String cod_despacho;
    private String um_pedido;
    private String factor;
    private Double precio_item;
    private String flag_bonificado;
    private String cod_linea;
    private Double porcentaje_isc ;
    private String um_control_stock;
    private Double porcentaje_igv;
    private Double porcentaje_cp;
    private Double porcentaje_cp_tc;
    private Double porcentaje_excep ;
    private Double porcentaje_item;
    private Double porcentaje_monto;
    private Double porcentaje_paquete;
    private Double porcentaje_tc;
    private Double porcentaje_vol;
    private Double descuento_cp;
    private Double descuento_cp_tc;
    private Double descuento_excep;
    private Double descuento_item;
    private Double descuento_monto;
    private Double descuento_tc;
    private Double descuento_vol;
    private Double descuento_paquete;
    private Double valor_venta;
    private Double impuesto_igv;
    private Double impuesto_selectivo;
    private Double venta_neta;
    private int qty_pedida;
    private int qty_atendida;
    private int qty_facturada;
    private int qty_despachada;
    private Double total_dscto;
    private String secuencia;
    private String desc_item;

    private String pedido_refacturar;

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }




    public String getPedido_refacturar() {
        return pedido_refacturar;
    }

    public void setPedido_refacturar(String pedido_refacturar) {
        this.pedido_refacturar = pedido_refacturar;
    }



    public String getNro_pedido() {
        return nro_pedido;
    }

    public void setNro_pedido(String nro_pedido) {
        this.nro_pedido = nro_pedido;
    }

    public String getNro_back_order() {
        return nro_back_order;
    }

    public void setNro_back_order(String nro_back_order) {
        this.nro_back_order = nro_back_order;
    }

    public String getCod_item() {
        return cod_item;
    }

    public void setCod_item(String cod_item) {
        this.cod_item = cod_item;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getCod_despacho() {
        return cod_despacho;
    }

    public void setCod_despacho(String cod_despacho) {
        this.cod_despacho = cod_despacho;
    }

    public String getUm_pedido() {
        return um_pedido;
    }

    public void setUm_pedido(String um_pedido) {
        this.um_pedido = um_pedido;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public Double getPrecio_item() {
        return precio_item;
    }

    public void setPrecio_item(Double precio_item) {
        this.precio_item = precio_item;
    }

    public String getFlag_bonificado() {
        return flag_bonificado;
    }

    public void setFlag_bonificado(String flag_bonificado) {
        this.flag_bonificado = flag_bonificado;
    }

    public String getCod_linea() {
        return cod_linea;
    }

    public void setCod_linea(String cod_linea) {
        this.cod_linea = cod_linea;
    }

    public Double getPorcentaje_isc() {
        return porcentaje_isc;
    }

    public void setPorcentaje_isc(Double porcentaje_isc) {
        this.porcentaje_isc = porcentaje_isc;
    }

    public String getUm_control_stock() {
        return um_control_stock;
    }

    public void setUm_control_stock(String um_control_stock) {
        this.um_control_stock = um_control_stock;
    }

    public Double getPorcentaje_igv() {
        return porcentaje_igv;
    }

    public void setPorcentaje_igv(Double porcentaje_igv) {
        this.porcentaje_igv = porcentaje_igv;
    }

    public Double getPorcentaje_cp() {
        return porcentaje_cp;
    }

    public void setPorcentaje_cp(Double porcentaje_cp) {
        this.porcentaje_cp = porcentaje_cp;
    }

    public Double getPorcentaje_cp_tc() {
        return porcentaje_cp_tc;
    }

    public void setPorcentaje_cp_tc(Double porcentaje_cp_tc) {
        this.porcentaje_cp_tc = porcentaje_cp_tc;
    }

    public Double getPorcentaje_excep() {
        return porcentaje_excep;
    }

    public void setPorcentaje_excep(Double porcentaje_excep) {
        this.porcentaje_excep = porcentaje_excep;
    }

    public Double getPorcentaje_item() {
        return porcentaje_item;
    }

    public void setPorcentaje_item(Double porcentaje_item) {
        this.porcentaje_item = porcentaje_item;
    }

    public Double getPorcentaje_monto() {
        return porcentaje_monto;
    }

    public void setPorcentaje_monto(Double porcentaje_monto) {
        this.porcentaje_monto = porcentaje_monto;
    }

    public Double getPorcentaje_paquete() {
        return porcentaje_paquete;
    }

    public void setPorcentaje_paquete(Double porcentaje_paquete) {
        this.porcentaje_paquete = porcentaje_paquete;
    }

    public Double getPorcentaje_tc() {
        return porcentaje_tc;
    }

    public void setPorcentaje_tc(Double porcentaje_tc) {
        this.porcentaje_tc = porcentaje_tc;
    }

    public Double getPorcentaje_vol() {
        return porcentaje_vol;
    }

    public void setPorcentaje_vol(Double porcentaje_vol) {
        this.porcentaje_vol = porcentaje_vol;
    }

    public Double getDescuento_cp() {
        return descuento_cp;
    }

    public void setDescuento_cp(Double descuento_cp) {
        this.descuento_cp = descuento_cp;
    }

    public Double getDescuento_cp_tc() {
        return descuento_cp_tc;
    }

    public void setDescuento_cp_tc(Double descuento_cp_tc) {
        this.descuento_cp_tc = descuento_cp_tc;
    }

    public Double getDescuento_excep() {
        return descuento_excep;
    }

    public void setDescuento_excep(Double descuento_excep) {
        this.descuento_excep = descuento_excep;
    }

    public Double getDescuento_item() {
        return descuento_item;
    }

    public void setDescuento_item(Double descuento_item) {
        this.descuento_item = descuento_item;
    }

    public Double getDescuento_monto() {
        return descuento_monto;
    }

    public void setDescuento_monto(Double descuento_monto) {
        this.descuento_monto = descuento_monto;
    }

    public Double getDescuento_tc() {
        return descuento_tc;
    }

    public void setDescuento_tc(Double descuento_tc) {
        this.descuento_tc = descuento_tc;
    }

    public Double getDescuento_vol() {
        return descuento_vol;
    }

    public void setDescuento_vol(Double descuento_vol) {
        this.descuento_vol = descuento_vol;
    }

    public Double getDescuento_paquete() {
        return descuento_paquete;
    }

    public void setDescuento_paquete(Double descuento_paquete) {
        this.descuento_paquete = descuento_paquete;
    }

    public Double getValor_venta() {
        return valor_venta;
    }

    public void setValor_venta(Double valor_venta) {
        this.valor_venta = valor_venta;
    }

    public Double getImpuesto_igv() {
        return impuesto_igv;
    }

    public void setImpuesto_igv(Double impuesto_igv) {
        this.impuesto_igv = impuesto_igv;
    }

    public Double getImpuesto_selectivo() {
        return impuesto_selectivo;
    }

    public void setImpuesto_selectivo(Double impuesto_selectivo) {
        this.impuesto_selectivo = impuesto_selectivo;
    }

    public Double getVenta_neta() {
        return venta_neta;
    }

    public void setVenta_neta(Double venta_neta) {
        this.venta_neta = venta_neta;
    }

    public int getQty_pedida() {
        return qty_pedida;
    }

    public void setQty_pedida(int qty_pedida) {
        this.qty_pedida = qty_pedida;
    }

    public int getQty_atendida() {
        return qty_atendida;
    }

    public void setQty_atendida(int qty_atendida) {
        this.qty_atendida = qty_atendida;
    }

    public int getQty_facturada() {
        return qty_facturada;
    }

    public void setQty_facturada(int qty_facturada) {
        this.qty_facturada = qty_facturada;
    }

    public int getQty_despachada() {
        return qty_despachada;
    }

    public void setQty_despachada(int qty_despachada) {
        this.qty_despachada = qty_despachada;
    }

    public Double getTotal_dscto() {
        return total_dscto;
    }

    public void setTotal_dscto(Double total_dscto) {
        this.total_dscto = total_dscto;
    }

    public String getDesc_item() {
        return desc_item;
    }

    public void setDesc_item(String desc_item) {
        this.desc_item = desc_item;
    }

}
