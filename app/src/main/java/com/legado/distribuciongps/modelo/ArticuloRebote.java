
package com.legado.distribuciongps.modelo;

import java.math.BigDecimal;

public class ArticuloRebote {
    private String codigo;
    private String descripcion;
    private String unidadMedida;
    private BigDecimal cantidad;
    private BigDecimal paquetes;
    private  BigDecimal unidades;
    private BigDecimal total;
    private String tipoRebote;

    public String getTipoRebote() {
        return tipoRebote;
    }

    public void setTipoRebote(String tipoRebote) {
        this.tipoRebote = tipoRebote;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPaquetes() {
        return paquetes;
    }

    public void setPaquetes(BigDecimal paquetes) {
        this.paquetes = paquetes;
    }

    public BigDecimal getUnidades() {
        return unidades;
    }

    public void setUnidades(BigDecimal unidades) {
        this.unidades = unidades;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
