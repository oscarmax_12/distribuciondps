package com.legado.distribuciongps.modelo;

import java.util.ArrayList;

/**
 * Created by __Adrian__ on 20/9/2017.
 * and Edited by Oscar 17/09/2019
 */
public class Packing {

    private String nro_packing;
    private String fecha;
    private Double monto_total;
    private boolean flag_checked;
    private String codAlmacen;
    private String codEmpresa;
    private String rutas;
    private String isHoy;
    private String cantDocsxPacking;
    private String cantDocsContxPacking;
    private String cantDocsCredxPacking;
    private String liquidado;
    public static ArrayList<Packing> listaPackings= new ArrayList<Packing>();

    public String getLiquidado() {
        return liquidado;
    }

    public void setLiquidado(String liquidado) {
        this.liquidado = liquidado;
    }

    public String getCantDocsxPacking() {
        return cantDocsxPacking;
    }

    public void setCantDocsxPacking(String cantDocsxPacking) {
        this.cantDocsxPacking = cantDocsxPacking;
    }

    public String getCantDocsContxPacking() {
        return cantDocsContxPacking;
    }

    public void setCantDocsContxPacking(String cantDocsContxPacking) {
        this.cantDocsContxPacking = cantDocsContxPacking;
    }

    public String getCantDocsCredxPacking() {
        return cantDocsCredxPacking;
    }

    public void setCantDocsCredxPacking(String cantDocsCredxPacking) {
        this.cantDocsCredxPacking = cantDocsCredxPacking;
    }

    public String getIsHoy() {
        return isHoy;
    }

    public void setIsHoy(String isHoy) {
        this.isHoy = isHoy;
    }

    public String getRutas() {
        return rutas;
    }

    public void setRutas(String rutas) {
        this.rutas = rutas;
    }

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public boolean isFlag_checked() {
        return flag_checked;
    }

    public void setFlag_checked(boolean flag_checked) {
        this.flag_checked = flag_checked;
    }

    public String getNro_packing() {
        return nro_packing;
    }

    public void setNro_packing(String nro_packing) {
        this.nro_packing = nro_packing;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(Double monto_total) {
        this.monto_total = monto_total;
    }
}
