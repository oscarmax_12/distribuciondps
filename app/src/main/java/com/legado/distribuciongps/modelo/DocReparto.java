package com.legado.distribuciongps.modelo;

/**
 * Created by ERICK on 16/08/2019.
 */

public class DocReparto {

    private String fechaEmision;
    private String codTipDoc;
    private String serie;
    private String impreso;
    private Double monto;
    private String codCliente;
    private String descCliente;
    private String packing;
    private String condPago;
    private String numPedido;
    private String codRuta;
    private String codVendedor;
    private String codEmpresa;
    private String codSede;
    private String estGuia;
    private String codAlmacen;
    private String dirCliente;
    private String codLocalidad;
    private String montoAnulado;
    private Integer secuencia;
    private String estDocumento;
    private Double saldo;
    private String estRefacturado;
    private String reboteDocumento;
    private String parCondicion;

    public String getParCondicion() {
        return parCondicion;
    }

    public void setParCondicion(String parCondicion) {
        this.parCondicion = parCondicion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getCodTipDoc() {
        return codTipDoc;
    }

    public void setCodTipDoc(String codTipDoc) {
        this.codTipDoc = codTipDoc;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getImpreso() {
        return impreso;
    }

    public void setImpreso(String impreso) {
        this.impreso = impreso;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getDescCliente() {
        return descCliente;
    }

    public void setDescCliente(String descCliente) {
        this.descCliente = descCliente;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getCondPago() {
        return condPago;
    }

    public void setCondPago(String condPago) {
        this.condPago = condPago;
    }

    public String getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(String numPedido) {
        this.numPedido = numPedido;
    }

    public String getCodRuta() {
        return codRuta;
    }

    public void setCodRuta(String codRuta) {
        this.codRuta = codRuta;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public String getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(String codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getCodSede() {
        return codSede;
    }

    public void setCodSede(String codSede) {
        this.codSede = codSede;
    }

    public String getEstGuia() {
        return estGuia;
    }

    public void setEstGuia(String estGuia) {
        this.estGuia = estGuia;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getDirCliente() {
        return dirCliente;
    }

    public void setDirCliente(String dirCliente) {
        this.dirCliente = dirCliente;
    }

    public String getCodLocalidad() {
        return codLocalidad;
    }

    public void setCodLocalidad(String codLocalidad) {
        this.codLocalidad = codLocalidad;
    }

    public String getMontoAnulado() {
        return montoAnulado;
    }

    public void setMontoAnulado(String montoAnulado) {
        this.montoAnulado = montoAnulado;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Integer secuencia) {
        this.secuencia = secuencia;
    }

    public String getEstDocumento() {
        return estDocumento;
    }

    public void setEstDocumento(String estDocumento) {
        this.estDocumento = estDocumento;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getEstRefacturado() {
        return estRefacturado;
    }

    public void setEstRefacturado(String estRefacturado) {
        this.estRefacturado = estRefacturado;
    }

    public String getReboteDocumento() {
        return reboteDocumento;
    }

    public void setReboteDocumento(String reboteDocumento) {
        this.reboteDocumento = reboteDocumento;
    }

}
