package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.ArticuloRebote;
import com.legado.distribuciongps.modelo.LogReparto;
import com.legado.distribuciongps.modelo.Pedido;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.modelo.Usuario;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface MetricasService {
    @GET("getMetricas")
    Call<JsonRespuesta> getMetricas(@QueryMap Map<String, String> parametros);
    @GET("getRebotesByPlaca")
    Call<JsonRespuesta<Rebote>> getRebotesByPlaca(@QueryMap Map<String, String> parametros);
    @GET("getEntregasByPlaca")
    Call<JsonRespuesta<Rebote>> getEntregasByPlaca(@QueryMap Map<String, String> parametros);

    @GET("getInventarioReboteByPlaca")
    Call<JsonRespuesta<ArticuloRebote>> getInventarioReboteByPlaca(@QueryMap Map<String, String> parametros);

    @POST("mantLogReparto")
    Call<JsonRespuesta<Integer>> mantLogReparto(@Body LogReparto logReparto);

    @POST("retornarAEntrega")
    Call<JsonRespuesta<Integer>> mantRetornarEntrega(@Body Rebote rebote);
    @GET("getPedidosByRefacturar")
    Call<JsonRespuesta<Pedido>> getPedidosByRefacturar(@QueryMap Map<String, String> parametros);


}
