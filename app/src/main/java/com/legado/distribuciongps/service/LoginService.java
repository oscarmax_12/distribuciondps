package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.Asignacion;
import com.legado.distribuciongps.modelo.Usuario;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by ERICK on 10/4/2017.
 */
public interface LoginService {

    @GET("loginDistrib")
    Call<JsonRespuesta<Usuario>> login(@QueryMap Map<String, String> parametros);
    @POST("iniciando")
    Call<JsonRespuesta<Asignacion>> CargarSedes();
    @GET("loginPlaca")
    Call<JsonRespuesta<Usuario>> loginxPlaca(@QueryMap Map<String, String> parametros);

}