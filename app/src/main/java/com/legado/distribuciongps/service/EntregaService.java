package com.legado.distribuciongps.service;

import com.legado.distribuciongps.json.JsonRespuesta;
import com.legado.distribuciongps.modelo.DocReparto;
import com.legado.distribuciongps.modelo.Entrega;
import com.legado.distribuciongps.modelo.Rebote;
import com.legado.distribuciongps.modelo.Version;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by OSCAR on 22/10/2019.
 */

public interface EntregaService {

    @POST("validarversion")
    Call<JsonRespuesta> ValidarVersion(@Body Version obj_version);

}
