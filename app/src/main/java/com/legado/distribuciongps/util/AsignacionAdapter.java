package com.legado.distribuciongps.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Asignacion;

import java.util.List;

/**
 * Created by ERICK on 06/06/2018.
 */

public class AsignacionAdapter extends ArrayAdapter<Asignacion> {

    private static final String TAG = "AsignacionAdapter";
    private List<Asignacion> asignaciones;
    private Activity activity;

    public AsignacionAdapter(Activity a, int textViewResourceId,List<Asignacion> asignaciones) {
        super(a, textViewResourceId, asignaciones);
        this.asignaciones = asignaciones;
        activity = a;
    }

    public static class ViewHolderEmpresaSede {
        public TextView descEmpresaSede;
    }

    ViewHolderEmpresaSede holder;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_empresasede, null);
            holder = new ViewHolderEmpresaSede();
            holder.descEmpresaSede = (TextView) v.findViewById(R.id.descEmpresa);
            v.setTag(holder);
        } else holder=(ViewHolderEmpresaSede) v.getTag();
        final Asignacion asignacion = asignaciones.get(position);
        if (asignacion != null) {
            holder.descEmpresaSede.setText(asignacion.getEmpresa().getDescEmpresa() + " - "+asignacion.getSede().getDescSede());
        }
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return  getView(position, convertView, parent);
    }

}