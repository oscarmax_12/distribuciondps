package com.legado.distribuciongps.util;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.DocReparto;

import java.util.List;

/**
 * Created by ERICK on 16/08/2019.
 */

public class AdapterDocReparto extends RecyclerView.Adapter<AdapterDocReparto.MyViewHolder> {

    private LayoutInflater inflater;
    private Context ctx;
    private FragmentActivity activity;
    private List<DocReparto> documentos;

    public AdapterDocReparto(FragmentActivity activity, Context ctx, List<DocReparto> documentos) {
        inflater = LayoutInflater.from(ctx);
        this.ctx = ctx;
        this.activity = activity;
        this.documentos = documentos;
    }

    @Override
    public AdapterDocReparto.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_documento, parent, false);
        AdapterDocReparto.MyViewHolder holder = new AdapterDocReparto.MyViewHolder(view);
        return holder;
    }

    public void refreshBlockOverlay(int position) {
        notifyItemChanged(position);
    }

    @Override
    public void onBindViewHolder(final AdapterDocReparto.MyViewHolder holder,final int position) {
        holder.txtNumDoc.setText((documentos.get(position).getCodTipDoc().equals("01") ? "F" : (documentos.get(position).getCodTipDoc().equals("02")?"B":"NN"))+documentos.get(position).getSerie() + "-"+documentos.get(position).getImpreso());
        holder.txtCodigo.setText(documentos.get(position).getDescCliente());
        holder.txtDescripcion.setText(documentos.get(position).getDirCliente());
        holder.txtPreUni.setText("CONDICION: " + ( documentos.get(position).getParCondicion().equals("1") ? "CRÉDITO" : (documentos.get(position).getParCondicion().equals("2") ? "CONTADO" : ("ADELANTADO")) ));
        holder.txtTotal.setText("MONTO: "+documentos.get(position).getMonto().toString());
    }

    @Override
    public int getItemCount() {
        return documentos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        //protected Button btnQuitar,btnEditar;
        public TextView txtCodigo, txtDescripcion, txtNumDoc, txtPreUni, txtTotal;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCodigo = (TextView) itemView.findViewById(R.id.txtCodigo);
            txtDescripcion = (TextView) itemView.findViewById(R.id.txtDescripcion);
            txtNumDoc = (TextView) itemView.findViewById(R.id.txtNumDoc);
            txtPreUni = (TextView) itemView.findViewById(R.id.txtPreUni);
            txtTotal = (TextView) itemView.findViewById(R.id.txtTotal);
            //btnQuitar = (Button) itemView.findViewById(R.id.btnQuitar);
            //btnEditar= (Button) itemView.findViewById(R.id.btnEditar);
        }
    }

}