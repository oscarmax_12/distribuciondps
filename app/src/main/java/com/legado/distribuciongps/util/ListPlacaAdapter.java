package com.legado.distribuciongps.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.Placa;
import java.util.List;

/**
 * Created by ERICK on 11/08/2018.
 */

public class ListPlacaAdapter extends BaseAdapter {

    private Activity activity;
    private List<Placa> placas;
    private Context context;

    public ListPlacaAdapter(Context context, List<Placa> placas, Activity activity) {
        this.activity = activity;
        this.placas = placas;
        this.context = context;
    }

    @Override
    public int getCount() {
        return placas.size();
    }

    @Override
    public Object getItem(int i) {
        return placas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent ) {
        final ListPlacaAdapter.ViewHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.fila_item_placa, parent, false);
            holder = new ListPlacaAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ListPlacaAdapter.ViewHolder) convertView.getTag();
        }
        Placa model = placas.get(position);
        /// model.setChecked(model.getEstado().equals("T"));
        holder.itemName.setText(model.getNumPlaca());
        //holder.ivCheckBox.setBackgroundResource(R.drawable.check);
        //if (model.getChecked()) {
         //   holder.ivCheckBox.setBackgroundResource(R.drawable.checked);
       // } else {
       //     holder.ivCheckBox.setBackgroundResource(R.drawable.check);
      //  }
        return convertView;
    }

    /*public void updateRecords(List<Categoria> categorias){
        this.categorias = categorias;
        notifyDataSetChanged();
    }*/

    private class ViewHolder{
        TextView itemName;
        TextView itemDescription;
        ImageView ivCheckBox;

        public ViewHolder(View view) {
            itemName = (TextView)view.findViewById(R.id.text_view_item_name);
            itemDescription = (TextView) view.findViewById(R.id.text_view_item_description);
            ivCheckBox = (ImageView) view.findViewById(R.id.iv_check_box);
        }
    }

    public List<Placa> getPlacas() {
        return placas;
    }

    public void setPlacas(List<Placa> placas) {
        this.placas = placas;
    }
}
