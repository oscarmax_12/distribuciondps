package com.legado.distribuciongps.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.legado.distribuciongps.R;
import com.legado.distribuciongps.modelo.TipoDocumento;

import java.util.List;

/**
 * Created by ERICK on 08/06/2018.
 */

public class TipoDocumentoAdapter extends ArrayAdapter<TipoDocumento> {

    private static final String TAG = "TipoDocumentoAdapter";
    private List<TipoDocumento> tipos;
    private Activity activity;

    public TipoDocumentoAdapter(Activity a, int textViewResourceId,List<TipoDocumento> tipos) {
        super(a, textViewResourceId, tipos);
        this.tipos = tipos;
        activity = a;
    }

    public static class ViewHolderTipDoc {
        public TextView descTipDoc;
    }

    ViewHolderTipDoc holder;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_empresasede, null);
            holder = new ViewHolderTipDoc();
            holder.descTipDoc = (TextView) v.findViewById(R.id.descEmpresa);
            v.setTag(holder);
        } else holder=(ViewHolderTipDoc) v.getTag();
        final TipoDocumento tipoDocumento = tipos.get(position);
        if (tipoDocumento != null) {
            holder.descTipDoc.setText(tipos.get(position).getNombre());
        }
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return  getView(position, convertView, parent);
    }

}