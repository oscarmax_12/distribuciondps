package com.legado.distribuciongps.util;

import com.legado.distribuciongps.modelo.Cliente;

import java.util.ArrayList;

public class PaqueteClientes {
    public ArrayList<Cliente> listaClientes;

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
}
