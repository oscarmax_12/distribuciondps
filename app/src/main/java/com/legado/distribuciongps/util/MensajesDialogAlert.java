package com.legado.distribuciongps.util;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;

import com.legado.distribuciongps.R;

public class MensajesDialogAlert {
    private static boolean mResult;
    private static String text;

    public static boolean mensajeConfirmacion(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        alert.setTitle(titleM);
        alert.setIcon(R.drawable.ic_help_orange_a700_48dp);
        alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
            }
        });
        alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mResult = false;
                handler.sendMessage(handler.obtainMessage());
            }
        });
        alert.setCancelable(false);
        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult;
    }

    public static String mensajeEditText(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };


        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        final EditText edittext = new EditText(context);
//        alert.setMessage("Confirmar");
//        alert.setTitle("Ingresar Motivo de Rebote");


        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        alert.setTitle(titleM);

        alert.setView(edittext);

        alert.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
//                Editable YouEditTextValue = edittext.getText();
                //OR
                text = edittext.getText().toString().trim();
                handler.sendMessage(handler.obtainMessage());
            }
        });

        alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
                text = null;
                handler.sendMessage(handler.obtainMessage());
            }
        });

        // make a text input dialog and show it
//        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        alert.setIcon(R.drawable.ic_help_orange_a700_48dp);
        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return text;
    }

    public static boolean mensajeInformacion(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        alert.setTitle(titleM);

        alert.setIcon(R.drawable.ic_info_outline_indigo_500_48dp);
        alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
            }
        });

        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult;
    }

    public static boolean mensajeError(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);


        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        alert.setTitle(titleM);
        alert.setCancelable(false);
        alert.setIcon(R.drawable.ic_highlight_off_red_700_36dp);
        alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
            }
        });

        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult;
    }

    public static boolean mensajeExito(Context context, String title, String message) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        //PARA CAMBIAR EL COLOR DEL MENSAJE DEL DIALOG
        SpannableString messageM = new SpannableString(message);
        messageM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, message.length(), 0);
        alert.setMessage(messageM);
        //PARA CAMBIAR EL COLOR  DEL TITULO DEL DIALOG
        SpannableString titleM = new SpannableString(title);
        titleM.setSpan(new ForegroundColorSpan(Color.BLACK), 0, title.length(), 0);
        alert.setTitle(titleM);
        alert.setIcon(R.drawable.ic_check_circle_green_700_36dp);
        alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mResult = true;
                handler.sendMessage(handler.obtainMessage());
            }
        });

        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException e2) {

        }

        return mResult;
    }
}
